from dependency_injector import containers, providers

from app.domain.authentication.use_cases import (
    GetActiveUserUseCase,
    LoginUseCase,
    RecoverPasswordUseCase,
    ResetPasswordUseCase,
)
from app.domain.interfaces.daos import (
    IFoodDAO, IUserDAO,
    IIntakeDAO, IWaterDAO,
    IUserFavouritesDAO,
)
from app.domain.interfaces.services import (
    IFoodService, IUserService,
    IUserSettingsService, IIntakeService,
    IWaterService, IUserFavouritesService,
)
from app.domain.services.favourites import UserFavouritesService
from app.domain.services.food import FoodService
from app.domain.services.intakes import IntakeService
from app.domain.services.user_settings import UserSettingsService
from app.domain.services.users import UserService
from app.domain.services.water import WaterService
from app.domain.use_cases.food import (
    SearchFoodUseCase
)
from app.infrastructure.daos.favourites import UserFavouritesDAO
from app.infrastructure.daos.food import FoodDAO
from app.infrastructure.daos.intakes import IntakeDAO
from app.infrastructure.daos.users import UserDAO
from app.infrastructure.daos.water import WaterDAO


class DataAccessObjects(containers.DeclarativeContainer):
    user: IUserDAO = providers.Singleton(UserDAO)
    food: IFoodDAO = providers.Singleton(FoodDAO)
    intake: IIntakeDAO = providers.Singleton(IntakeDAO)
    water: IWaterDAO = providers.Singleton(WaterDAO)
    user_favourites: IUserFavouritesDAO = providers.Singleton(UserFavouritesDAO)


class Services(containers.DeclarativeContainer):
    _food_service = providers.Singleton(FoodService, DataAccessObjects.food)
    _user_service = providers.Singleton(UserService, DataAccessObjects.user)
    _user_settings_service = providers.Singleton(UserSettingsService, DataAccessObjects.user)
    _intake_service = providers.Singleton(IntakeService, DataAccessObjects.user,
                                          DataAccessObjects.intake, DataAccessObjects.food)
    _water_service = providers.Singleton(WaterService, DataAccessObjects.water)
    _user_favourites_service = providers.Singleton(UserFavouritesService,
                                                   DataAccessObjects.user_favourites)

    @classmethod
    def get_food_service(cls) -> IFoodService:
        return cls._food_service()

    @classmethod
    def get_user_service(cls) -> IUserService:
        return cls._user_service()

    @classmethod
    def get_user_settings_service(cls) -> IUserSettingsService:
        return cls._user_settings_service()

    @classmethod
    def get_intake_service(cls) -> IIntakeService:
        return cls._intake_service()

    @classmethod
    def get_water_service(cls) -> IWaterService:
        return cls._water_service()

    @classmethod
    def get_user_favourites_service(cls) -> IUserFavouritesService:
        return cls._user_favourites_service()


class UseCases(containers.DeclarativeContainer):
    # User use cases
    get_active_user = providers.Factory(GetActiveUserUseCase,
                                        DataAccessObjects.user)
    login = providers.Factory(LoginUseCase,
                              DataAccessObjects.user)
    recover_password = providers.Factory(RecoverPasswordUseCase,
                                         DataAccessObjects.user)
    reset_password = providers.Factory(ResetPasswordUseCase,
                                       DataAccessObjects.user)
    # Food use cases
    search_food = providers.Factory(SearchFoodUseCase,
                                    DataAccessObjects.food)


use_cases = UseCases
