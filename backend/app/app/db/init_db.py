import logging

from rethinkdb import RethinkDB
from rethinkdb.errors import ReqlOpFailedError

from app import config
from app.config import DB_HOST, DB_PORT, DB_NAME
from app.domain.entities.role import RoleEnum
from app.domain.entities.user import UserCreate
from app.infrastructure.daos.users import UserDAO


def init_db():
    logging.info("Before connecting to database")
    r = RethinkDB()
    connection = r.connect(host=DB_HOST, port=DB_PORT)
    logging.info("After connecting to database")
    logging.info("Before creating database")
    db_list = r.db_list().run(connection)
    if DB_NAME not in db_list:
        r.db_create(DB_NAME).run(connection)
        connection.use(DB_NAME)
    logging.info("Before creating tables")
    try:
        r.table_create('users').run(connection)
    except ReqlOpFailedError:
        pass
    try:
        r.table_create('intakes').run(connection)
    except ReqlOpFailedError:
        pass
    try:
        r.table_create('food').run(connection)
    except ReqlOpFailedError:
        pass
    logging.info("After creating tables")

    logging.info("Before creating indexes")
    index_tables = {
        'users': {
            'email': lambda: r.table('users').index_create('email').run(connection),
        },
        'food': {
            'state': lambda: r.table('food').index_create('state', r.row['metadata']['state']).run(connection),
            'name': lambda: r.table('food').index_create('name').run(connection),
        }
    }
    for table, indexes in index_tables.items():
        existed_indexes = r.table(table).index_list().run(connection)
        for index, func in indexes.items():
            if index not in existed_indexes:
                func()

    r.table('users').index_wait('email').run(connection)
    r.table('food').index_wait('state').run(connection)
    r.table('food').index_wait('name').run(connection)

    logging.info("After creating indexes")

    logging.info("Before creating first superuser")
    user_in = UserCreate(
        email=config.FIRST_SUPERUSER,
        password=config.FIRST_SUPERUSER_PASSWORD,
        role=RoleEnum.superuser,
    )
    user_dao = UserDAO()
    if not user_dao.get_by_email(email=user_in.email):
        user_dao.create(user_in=user_in)
    logging.info("After creating first superuser")
