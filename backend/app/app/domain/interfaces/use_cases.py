from abc import abstractmethod, ABC


class IUseCase(ABC):

    @abstractmethod
    def execute(self, *args, **kwargs):
        pass
