from abc import abstractmethod, ABCMeta
from datetime import date
from typing import List

from pydantic import EmailStr

from app.domain.dtos import FoodFilterEntity
from app.domain.entities import (
    FoodDetailEntity, UserInDB,
    UserCreate, User,
    UserUpdate, FoodCreateEntity
)
from app.domain.entities.food.food import FoodUpdateEntity
from app.domain.entities.food.measure import WaterMeasureEntity
from app.domain.entities.intakes import IntakeListEntity, WaterIntakeEntity


class IFoodService(metaclass=ABCMeta):
    @abstractmethod
    def create(self, food_in: FoodCreateEntity, user: UserInDB) -> FoodDetailEntity:
        pass

    @abstractmethod
    def delete(self, food_id: str, user: UserInDB) -> bool:
        pass

    @abstractmethod
    def get_detail(self, food_id: str, user: UserInDB) -> FoodDetailEntity:
        pass

    @abstractmethod
    def get_list(self, user: UserInDB, filter_: FoodFilterEntity, skip: int, limit: int) -> List[FoodDetailEntity]:
        pass

    @abstractmethod
    def update(self, food_in: FoodUpdateEntity, user: UserInDB) -> FoodDetailEntity:
        pass

    @abstractmethod
    def validate(self, food_in: FoodDetailEntity, user: UserInDB) -> FoodDetailEntity:
        pass


class IUserService(metaclass=ABCMeta):

    @abstractmethod
    def create(self, user_in: UserCreate) -> User:
        pass

    @abstractmethod
    def get_detail(self, user_id: str, current_user: UserInDB) -> UserInDB:
        pass

    @abstractmethod
    def get_list(self, current_user: UserInDB, skip: int, limit: int) -> List[User]:
        pass

    @abstractmethod
    def register(self, email: EmailStr, password: str, first_name: str = None, last_name: str = None):
        pass

    @abstractmethod
    def update(self, user_id: str, user_to_update: UserUpdate, current_user: UserInDB) -> UserInDB:
        pass

    @abstractmethod
    def search(self, query_string: str, skip: int, limit: int) -> List[User]:
        pass


class IUserSettingsService(metaclass=ABCMeta):

    @abstractmethod
    def create_intake(self, user_id: str, intake: str) -> List[str]:
        pass

    @abstractmethod
    def get_intakes(self, user_id: str) -> List[str]:
        pass

    @abstractmethod
    def update_intakes(self, user_id: str, intakes: List[str]) -> List[str]:
        pass

    @abstractmethod
    def create_water_measure(self, user_id: str, measure: WaterMeasureEntity) -> WaterMeasureEntity:
        pass

    @abstractmethod
    def get_water_measures(self, user_id: str) -> List[WaterMeasureEntity]:
        pass

    @abstractmethod
    def update_water_measure(self, user_id: str, measure_name: str, measure: WaterMeasureEntity) -> WaterMeasureEntity:
        pass

    @abstractmethod
    def delete_water_measure(self, user_id: str, measure_name: str) -> bool:
        pass

    @abstractmethod
    def get_water_measure(self, user_id: str, measure_name: str) -> WaterMeasureEntity:
        pass


class IIntakeService(metaclass=ABCMeta):

    @abstractmethod
    def get_intake_by_day(self, user_id: str, intake_date: date) -> IntakeListEntity:
        """Getting intake for user by date"""

    @abstractmethod
    def get_intakes_by_date_range(self, user_id: str,
                                  intake_date_start: date, intake_date_end: date) -> List[IntakeListEntity]:
        """Getting list of intakes for user by date range"""

    @abstractmethod
    def add_product_to_intake(self, user_id: str, intake_date: date, intake_name: str,
                              food_id: str, portion_size: float) -> IntakeListEntity:
        """Adding product to intake"""

    @abstractmethod
    def remove_product_from_intake(self, user_id: str, intake_date: date,
                                   intake_name: str, food_id: str) -> IntakeListEntity:
        """Remove product from intake"""

    @abstractmethod
    def update_product_in_intake(self, user_id: str, intake_date: date, intake_name: str,
                                 food_id: str, portion_size: float) -> IntakeListEntity:
        """Update product in intake"""


class IWaterService(metaclass=ABCMeta):

    @abstractmethod
    def add_water(self, *, user_id: str, water_date: date, water_amount: int) -> int:
        """Add water to daily usage"""

    @abstractmethod
    def remove_water(self, *, user_id: str, water_date: date, water_amount: int) -> bool:
        """Remove water from daily usage"""

    @abstractmethod
    def get_daily_water(self, *, user_id: str, water_date: date) -> WaterIntakeEntity:
        """Get daily water consumption"""


class IUserFavouritesService(metaclass=ABCMeta):

    @abstractmethod
    def get_user_favourites(self, *, user_id: str) -> List[FoodDetailEntity]:
        """Get all user favourite food"""

    @abstractmethod
    def create_user_favourite(self, *, user_id: str, food_id: str) -> FoodDetailEntity:
        """Add new favourite to user favourites"""

    @abstractmethod
    def remove_user_favourite(self, *, user_id: str, food_id: str) -> bool:
        """Remove food from user favourites"""
