from abc import abstractmethod, ABCMeta
from datetime import date
from typing import Optional, List

from app.domain.constants import FoodStateEnum
from app.domain.dtos import FoodFilterEntity
from app.domain.entities import (
    FoodDetailEntity,
    FoodBaseDBEntity,
    UserUpdate,
    UserInDB,
    UserCreate,
    FoodCreateEntity
)
from app.domain.entities.food.food import FoodUpdateEntity
from app.domain.entities.food.measure import WaterMeasureEntity
from app.domain.entities.intakes import IntakeListEntity, WaterIntakeEntity


class IUserDAO(metaclass=ABCMeta):

    @abstractmethod
    def create(self, *, user_in: UserCreate):
        """Creating new user"""

    @abstractmethod
    def get(self, *, id_: str) -> Optional[UserInDB]:
        """Getting user details by user id"""

    @abstractmethod
    def get_by_email(self, *, email: str) -> Optional[UserInDB]:
        """Getting user details by email"""

    @abstractmethod
    def update(self, *, user_id: str, user_in: UserUpdate) -> Optional[UserInDB]:
        """Updating user profile"""

    @abstractmethod
    def get_user_list(self, *, skip=0, limit=100):
        """Getting list of users"""

    @abstractmethod
    def search(self, *, query_string: str, skip=0, limit=100):
        """Searching users by query string"""

    @abstractmethod
    def add_intake(self, *, user_id: str, intake: str) -> List[str]:
        """Adding new intake to user intakes"""

    @abstractmethod
    def get_intakes(self, *, user_id: str) -> List[str]:
        """Getting intakes, configured in user profile"""

    @abstractmethod
    def update_intakes(self, *, user_id: str, intakes: List[str]) -> List[str]:
        """Updating user intakes configuration"""

    @abstractmethod
    def create_water_measure(self, *, user_id: str, measure: WaterMeasureEntity) -> WaterMeasureEntity:
        """Creating water measure"""

    @abstractmethod
    def get_water_measures(self, *, user_id: str) -> List[WaterMeasureEntity]:
        """Getting list of all water measures"""

    @abstractmethod
    def update_water_measure(self, *, user_id: str, measure_name: str,
                             measure: WaterMeasureEntity) -> WaterMeasureEntity:
        """Update water measure"""

    @abstractmethod
    def delete_water_measure(self, *, user_id: str, measure_name: str) -> bool:
        """Delete measure from measures"""

    @abstractmethod
    def get_water_measure(self, *, user_id: str, measure_name: str) -> Optional[WaterMeasureEntity]:
        """Get detail info about water measure"""


class IFoodDAO(metaclass=ABCMeta):

    @abstractmethod
    def create(self, *, food_in: FoodCreateEntity, username: str) -> FoodDetailEntity:
        """
        Create new food entity with username of creator.
        :param food_in: entity of food
        :param username: name of user, who created this product
        :return: created food entity
        """

    @abstractmethod
    def get(self, food_id: str) -> FoodDetailEntity:
        """
        Get food details by id
        :param food_id:
        :return:
        """

    @abstractmethod
    def get_by_name(self, name: str) -> Optional[FoodDetailEntity]:
        pass

    @abstractmethod
    def get_list(self, *, user_email: str, skip=0, limit=100,
                 food_filter: FoodFilterEntity) -> List[FoodDetailEntity]:
        """
        Getting food list for users
        """

    @abstractmethod
    def search(self, *, query_string: str, skip=0, limit=100) -> List[FoodBaseDBEntity]:
        pass

    @abstractmethod
    def update(self, *, food_update: FoodUpdateEntity, state: FoodStateEnum = None) -> FoodDetailEntity:
        pass

    @abstractmethod
    def delete(self, food_id: str) -> bool:
        pass


class IIntakeDAO(metaclass=ABCMeta):

    @abstractmethod
    def create(self, *, user_id: str, intakes: IntakeListEntity) -> IntakeListEntity:
        """Create intake"""

    @abstractmethod
    def get(self, *, user_id: str, intake_date: date) -> IntakeListEntity:
        """Get List of daily intakes"""

    @abstractmethod
    def get_user_intakes_in_range(self, *, user_id: str,
                                  intake_date_from: date, intake_date_to: date) -> List[IntakeListEntity]:
        """Get intakes in date range"""

    @abstractmethod
    def update(self, *, user_id: str, intakes: IntakeListEntity) -> IntakeListEntity:
        """Updates user intake entity"""


class IWaterDAO(metaclass=ABCMeta):

    @abstractmethod
    def add_water(self, *, user_id: str, water_date: date, water_amount: int) -> int:
        """Add water to daily usage"""

    @abstractmethod
    def remove_water(self, *, user_id: str, water_date: date, water_amount: int) -> bool:
        """Remove water from daily usage"""

    @abstractmethod
    def get_daily_water(self, *, user_id: str, water_date: date) -> WaterIntakeEntity:
        """Get daily water consumption"""


class IUserFavouritesDAO(metaclass=ABCMeta):

    @abstractmethod
    def get_user_favourites(self, *, user_id: str) -> List[FoodDetailEntity]:
        """Get list of user favourites"""

    @abstractmethod
    def add_user_favourite(self, *, user_id: str, food_id: str) -> FoodDetailEntity:
        """Add new food to user favourite"""

    @abstractmethod
    def remove_user_favourite(self, *, user_id: str, food_id: str) -> bool:
        """Remove food from user favourites"""
