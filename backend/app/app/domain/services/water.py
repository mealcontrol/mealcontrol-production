from datetime import date

from app.domain.entities.intakes import WaterIntakeEntity
from app.domain.interfaces.daos import IWaterDAO
from app.domain.interfaces.services import IWaterService


class WaterService(IWaterService):
    def __init__(self, water_dao: IWaterDAO):
        self._water_dao = water_dao

    def add_water(self, *, user_id: str, water_date: date, water_amount: int) -> int:
        return self._water_dao.add_water(user_id=user_id, water_date=water_date, water_amount=water_amount)

    def remove_water(self, *, user_id: str, water_date: date, water_amount: int) -> bool:
        return self._water_dao.remove_water(user_id=user_id, water_date=water_date, water_amount=water_amount)

    def get_daily_water(self, *, user_id: str, water_date: date) -> WaterIntakeEntity:
        return self._water_dao.get_daily_water(user_id=user_id, water_date=water_date)