from typing import List

from app.domain.entities.food.measure import WaterMeasureEntity
from app.domain.interfaces.daos import IUserDAO
from app.domain.interfaces.services import IUserSettingsService


class UserSettingsService(IUserSettingsService):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def create_intake(self, user_id: str, intake: str) -> List[str]:
        return self._user_dao.add_intake(user_id=user_id, intake=intake)

    def get_intakes(self, user_id: str) -> List[str]:
        return self._user_dao.get_intakes(user_id=user_id)

    def update_intakes(self, user_id: str, intakes: List[str]) -> List[str]:
        return self._user_dao.update_intakes(user_id=user_id, intakes=intakes)

    def create_water_measure(self, user_id: str, measure: WaterMeasureEntity) -> WaterMeasureEntity:
        return self._user_dao.create_water_measure(user_id=user_id, measure=measure)

    def get_water_measures(self, user_id: str) -> List[WaterMeasureEntity]:
        return self._user_dao.get_water_measures(user_id=user_id)

    def update_water_measure(self, user_id: str, measure_name: str, measure: WaterMeasureEntity) -> WaterMeasureEntity:
        return self._user_dao.update_water_measure(user_id=user_id, measure_name=measure_name, measure=measure)

    def delete_water_measure(self, user_id: str, measure_name: str) -> bool:
        return self._user_dao.delete_water_measure(user_id=user_id, measure_name=measure_name)

    def get_water_measure(self, user_id: str, measure_name: str) -> WaterMeasureEntity:
        return self._user_dao.get_water_measure(user_id=user_id, measure_name=measure_name)
