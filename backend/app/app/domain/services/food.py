from typing import List

from fastapi import HTTPException

from app.domain.authentication.helpers import is_superuser
from app.domain.constants import FoodStateEnum
from app.domain.dtos import FoodFilterEntity
from app.domain.entities import FoodDetailEntity, UserInDB, FoodCreateEntity
from app.domain.entities.food.food import FoodUpdateEntity
from app.domain.interfaces.daos import IFoodDAO
from app.domain.interfaces.services import IFoodService


class FoodService(IFoodService):
    def __init__(self, food_dao: IFoodDAO):
        self._food_dao = food_dao

    def create(self, food_in: FoodCreateEntity, user: UserInDB) -> FoodDetailEntity:
        if is_superuser(user):
            username = food_in.owner_email
        else:
            username = user.email
        food = self._food_dao.create(food_in=food_in, username=username)
        return food

    def delete(self, food_id: str, user: UserInDB) -> bool:
        food_in_db = self._food_dao.get(food_id)
        if not food_in_db:
            raise HTTPException(
                status_code=400,
                detail="The food was already deleted from system.",
            )

        if food_in_db.owner_email == user.email or is_superuser(user):
            result = self._food_dao.delete(food_id)
            return result

        raise HTTPException(
            status_code=403,
            detail="You have no permissions to delete this food.",
        )

    def get_detail(self, food_id: str, user: UserInDB) -> FoodDetailEntity:
        food = self._food_dao.get(food_id=food_id)
        if not food:
            raise HTTPException(
                status_code=400,
                detail="The food with this name was not found.",
            )
        if food.owner_email != user.email and not is_superuser(user):
            raise HTTPException(
                status_code=400,
                detail="You have no permissions to get info about this food."
            )
        return food

    def get_list(self, user: UserInDB, filter_: FoodFilterEntity, skip: int, limit: int) -> List[FoodDetailEntity]:
        username = None
        if not is_superuser(user):
            username = user.email
        food_list = self._food_dao.get_list(
            user_email=username,
            skip=skip,
            limit=limit,
            food_filter=filter_
        )
        return food_list

    def update(self, food_in: FoodUpdateEntity, user: UserInDB) -> FoodDetailEntity:
        food = self._food_dao.get(food_id=food_in.id)

        if not food:
            raise HTTPException(
                status_code=404,
                detail="The food with this food id does not exist in the system",
            )

        if food.owner_email != user.email and not is_superuser(user):
            raise HTTPException(
                status_code=400,
                detail="You have no permissions to update this food."
            )
        food = self._food_dao.update(food_update=food_in)
        return food

    def validate(self, food_in: FoodDetailEntity, user: UserInDB) -> FoodDetailEntity:
        _validation_pipeline = [
            FoodStateEnum.NEW,
            FoodStateEnum.READY_FOR_REVIEW,
            FoodStateEnum.IN_REVIEW,
            FoodStateEnum.VERIFIED
        ]

        food_state = food_in.metadata.state
        if food_state != FoodStateEnum.VERIFIED:
            next_state = _validation_pipeline[_validation_pipeline.index(food_state) + 1]
            food_in.metadata.state = next_state

        return self.update(food_in=food_in, user=user)
