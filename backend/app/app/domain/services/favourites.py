from typing import List

from app.domain.entities import FoodDetailEntity
from app.domain.interfaces.daos import IUserFavouritesDAO
from app.domain.interfaces.services import IUserFavouritesService


class UserFavouritesService(IUserFavouritesService):
    def __init__(self, user_favourites_dao: IUserFavouritesDAO):
        self._dao = user_favourites_dao

    def get_user_favourites(self, *, user_id: str) -> List[FoodDetailEntity]:
        return self._dao.get_user_favourites(user_id=user_id)

    def create_user_favourite(self, *, user_id: str, food_id: str) -> FoodDetailEntity:
        return self._dao.add_user_favourite(user_id=user_id, food_id=food_id)

    def remove_user_favourite(self, *, user_id: str, food_id: str) -> bool:
        return self._dao.remove_user_favourite(user_id=user_id, food_id=food_id)
