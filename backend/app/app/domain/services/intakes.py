from datetime import date
from typing import List

from fastapi import HTTPException

from app.config import DEFAULT_PORTION_SIZE
from app.domain.entities.food.food import FoodIntakeEntity
from app.domain.entities.intakes import IntakeListEntity, IntakeDetailEntity, ProductIntakeEntity
from app.domain.interfaces.daos import IUserDAO, IIntakeDAO, IFoodDAO
from app.domain.interfaces.services import IIntakeService


class IntakeService(IIntakeService):
    def __init__(self, user_dao: IUserDAO, intake_dao: IIntakeDAO, food_dao: IFoodDAO):
        self._user_dao = user_dao
        self._intake_dao = intake_dao
        self._food_dao = food_dao
        super().__init__()

    def get_intake_by_day(self, user_id: str, intake_date: date) -> IntakeListEntity:
        daily_intakes = self._intake_dao.get(user_id=user_id, intake_date=intake_date)

        if not daily_intakes:
            daily_intakes = self._get_default_daily_intakes(user_id, intake_date)

        return daily_intakes

    def get_intakes_by_date_range(self, user_id: str,
                                  intake_date_start: date, intake_date_end: date) -> List[IntakeListEntity]:
        return self._intake_dao.get_user_intakes_in_range(
            user_id=user_id, intake_date_from=intake_date_start, intake_date_to=intake_date_end
        )

    def add_product_to_intake(self, user_id: str, intake_date: date, intake_name: str, food_id: str,
                              portion_size: float) -> IntakeListEntity:
        product_entity = self._food_dao.get(food_id=food_id)
        if not product_entity:
            raise HTTPException(status_code=404, detail=f"Food with id {food_id} doesn't exist")

        intakes_by_date = self.get_intake_by_day(user_id=user_id, intake_date=intake_date)

        if intake_name not in [intake.intake_name for intake in intakes_by_date.intakes]:
            if intake_name in self._user_dao.get_intakes(user_id=user_id):
                intakes_by_date.intakes.append(
                    IntakeDetailEntity(
                        intake_name=intake_name
                    )
                )
            else:
                raise HTTPException(status_code=400, detail=f"Intake with name {intake_name} does not exist")

        for intake in intakes_by_date.intakes:
            if intake.intake_name == intake_name:
                if food_id in [product.product.id for product in intake.products]:
                    raise HTTPException(status_code=400, detail="Product was already added!")
                intake.products.append(
                    ProductIntakeEntity(
                        product=FoodIntakeEntity(**product_entity.dict()),
                        portion_size=portion_size
                    )
                )
        intakes_by_date = self._recalculate_intake_meta(intakes_by_date)

        return self._intake_dao.update(user_id=user_id, intakes=intakes_by_date)

    def remove_product_from_intake(self, user_id: str, intake_date: date, intake_name: str,
                                   food_id: str) -> IntakeListEntity:
        intakes_by_date = self.get_intake_by_day(user_id, intake_date)

        if intake_name not in [intake.intake_name for intake in intakes_by_date.intakes]:
            raise HTTPException(status_code=400, detail=f"Intake with name {intake_name} does not exist")

        for intake in intakes_by_date.intakes:
            if intake.intake_name == intake_name:
                new_products_list = [product for product in intake.products if product.product.id != food_id]
                if len(new_products_list) == len(intake.products):
                    raise HTTPException(status_code=404, detail=f"Food with id {food_id} does not inside intake")
                intake.products = new_products_list
                break
        intakes_by_date = self._recalculate_intake_meta(intakes_by_date)

        return self._intake_dao.update(user_id=user_id, intakes=intakes_by_date)

    def update_product_in_intake(self, user_id: str, intake_date: date, intake_name: str, food_id: str,
                                 portion_size: float) -> IntakeListEntity:
        intakes_by_date = self.get_intake_by_day(user_id, intake_date)

        if intake_name not in [intake.intake_name for intake in intakes_by_date.intakes]:
            raise HTTPException(status_code=400, detail=f"Intake with name {intake_name} does not exist")

        for intake in intakes_by_date.intakes:
            if intake.intake_name == intake_name:
                for product in intake.products:
                    if product.product.id == food_id:
                        product.portion_size = portion_size
                        break
                break
        intakes_by_date = self._recalculate_intake_meta(intakes_by_date)

        return self._intake_dao.update(user_id=user_id, intakes=intakes_by_date)

    def _get_default_daily_intakes(self, user_id: str, intake_date: date):
        intakes_by_user = self._user_dao.get_intakes(user_id=user_id)
        daily_intakes = IntakeListEntity(
            date_=intake_date,
            owner_id=user_id,
            intakes=[IntakeDetailEntity(intake_name=intake_name) for intake_name in intakes_by_user]
        )
        return daily_intakes

    @staticmethod
    def _recalculate_intake_meta(intakes: IntakeListEntity) -> IntakeListEntity:
        intakes.calories = 0
        intakes.fats = 0
        intakes.carbs = 0
        intakes.proteins = 0
        for intake in intakes.intakes:
            intake.calories = 0
            intake.proteins = 0
            intake.fats = 0
            intake.carbs = 0
            for product in intake.products:
                intake.calories += round(product.product.calories / DEFAULT_PORTION_SIZE * product.portion_size)
                intake.proteins += product.product.proteins / DEFAULT_PORTION_SIZE * product.portion_size
                intake.fats += product.product.fats / DEFAULT_PORTION_SIZE * product.portion_size
                intake.carbs += product.product.carbs / DEFAULT_PORTION_SIZE * product.portion_size
            intakes.calories += intake.calories
            intakes.proteins += intake.proteins
            intakes.fats += intake.fats
            intakes.carbs += intake.carbs
        return intakes
