from typing import List

from fastapi import HTTPException
from pydantic import EmailStr

from app import config
from app.domain.authentication.helpers import is_superuser
from app.domain.entities import UserCreate, User, UserInDB, UserUpdate
from app.domain.interfaces.daos import IUserDAO
from app.domain.interfaces.services import IUserService
from app.utils import send_new_account_email


class UserService(IUserService):
    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def create(self, user_in: UserCreate) -> User:
        user = self._user_dao.get_by_email(email=user_in.email)
        if user:
            raise HTTPException(
                status_code=400,
                detail="The user with this email already exists in the system.",
            )
        user = self._user_dao.create(user_in=user_in)

        if config.EMAILS_ENABLED and user.email:
            send_new_account_email(
                email_to=user_in.email,
                password=user_in.password
            )
        return user

    def get_detail(self, user_id: str, current_user: UserInDB) -> UserInDB:
        user = self._user_dao.get(id_=user_id)
        if user == current_user:
            return user
        if not is_superuser(current_user):
            raise HTTPException(
                status_code=400, detail="The user doesn't have enough privileges"
            )
        return user

    def get_list(self, current_user: UserInDB, skip: int, limit: int) -> List[User]:
        if not is_superuser(current_user):
            raise HTTPException(
                status_code=500, detail="The user doesn't have enough privileges"
            )
        users = self._user_dao.get_user_list(skip=skip, limit=limit)
        return users

    def register(self, email: EmailStr, password: str, first_name: str = None, last_name: str = None):
        if not config.USERS_OPEN_REGISTRATION:
            raise HTTPException(
                status_code=403,
                detail="Open user registration is forbidden on this server",
            )
        user = self._user_dao.get_by_email(email=email)
        if user:
            raise HTTPException(
                status_code=400,
                detail="The user with this username already exists in the system",
            )
        user_in = UserCreate(
            password=password,
            email=email,
            first_name=first_name,
            last_name=last_name
        )
        user = self._user_dao.create(user_in=user_in)
        if config.EMAILS_ENABLED and user_in.email:
            send_new_account_email(
                email_to=user_in.email, password=user_in.password
            )
        return user

    def update(self, user_id: str, user_to_update: UserUpdate, current_user: UserInDB) -> UserInDB:
        if not is_superuser(current_user) \
                and (user_to_update.email is not None and user_to_update.email != current_user.email):
            raise HTTPException(
                status_code=400,
                detail="The user doesn't has enough privileges"
            )
        if is_superuser(current_user) and user_to_update.email is not None:
            user_email = self._user_dao.get_by_email(email=user_to_update.email)
            if not user_email:
                raise HTTPException(
                    status_code=404,
                    detail=f"The user with email {user_to_update.email} was not found it the system"
                )
        updated_user = self._user_dao.update(user_id=user_id, user_in=user_to_update)
        if updated_user:
            return updated_user
        raise HTTPException(status_code=400, detail="User has not been updated")

    def search(self, query_string: str, skip: int, limit: int) -> List[User]:
        users = self._user_dao.search(
            query_string=query_string,
            skip=skip,
            limit=limit
        )
        return users
