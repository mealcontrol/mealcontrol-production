from datetime import timedelta

from fastapi import HTTPException

from app import config
from app.domain.authentication.helpers import is_active
from app.domain.authentication.jwt import create_access_token
from app.domain.authentication.security import verify_password
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.token import TokenEntity


class LoginUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def execute(self,
                email: str,
                password: str
                ) -> TokenEntity:

        user = self._user_dao.get_by_email(email=email)
        if not user:
            raise HTTPException(status_code=400, detail="Incorrect email")
        if not verify_password(password, user.hashed_password):
            raise HTTPException(status_code=400, detail="Incorrect password")
        if not is_active(user):
            raise HTTPException(status_code=400, detail="Inactive user")
        access_token_expires = timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
        result = TokenEntity(
            access_token=create_access_token(
                data={"username": user.email},
                expires_delta=access_token_expires
            ),
            token_type="bearer"
        )
        return result
