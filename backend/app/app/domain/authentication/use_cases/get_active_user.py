import jwt
from fastapi import Security, HTTPException
from jwt import PyJWTError
from starlette.status import HTTP_403_FORBIDDEN

from app import config
from app.domain.authentication.helpers import is_active
from app.domain.authentication.jwt import ALGORITHM
from app.domain.authentication.security import reusable_oauth2
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.token import TokenPayloadEntity
from app.domain.entities.user import UserInDB


class GetActiveUserUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def execute(self,
                token: str = Security(reusable_oauth2)
                ) -> UserInDB:
        try:
            payload = jwt.decode(token, config.SECRET_KEY, algorithms=[ALGORITHM])
            token_data = TokenPayloadEntity(**payload)
        except PyJWTError:
            raise HTTPException(
                status_code=HTTP_403_FORBIDDEN, detail="Could not validate credentials"
            )
        user = self._user_dao.get_by_email(email=token_data.username)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")

        if not is_active(user):
            raise HTTPException(status_code=400, detail="Inactive user")

        return user
