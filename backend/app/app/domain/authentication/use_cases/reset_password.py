from fastapi import HTTPException

from app.domain.authentication.helpers import verify_password_reset_token, is_active
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.user import UserUpdate


class ResetPasswordUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def execute(self,
                token: str,
                new_password: str
                ) -> bool:

        username = verify_password_reset_token(token)
        if not username:
            raise HTTPException(status_code=400, detail="Invalid token")
        user = self._user_dao.get(username=username)
        if not user:
            raise HTTPException(
                status_code=404,
                detail="The user with this username does not exist in the system.",
            )
        elif not is_active(user):
            raise HTTPException(status_code=400, detail="Inactive user")
        user_in = UserUpdate(name=username, password=new_password)
        self._user_dao.update(username=username, user_in=user_in)
        return True
