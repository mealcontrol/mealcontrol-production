from enum import Enum, auto

from app.config import ROLE_SUPERUSER, ROLE_USER


class UseCaseResponseStatusEnum(Enum):
    SUCCESS = auto()
    RESOURCE_ERROR = auto()
    PARAMETERS_ERROR = auto()
    INTERNAL_ERROR = auto()


class RoleEnum(Enum):
    superuser = ROLE_SUPERUSER
    user = ROLE_USER


class LanguageEnum(Enum):
    ru = "ru"
    en = "en"


class FoodStateEnum(Enum):
    NEW = "new"
    READY_FOR_REVIEW = "ready for review"
    IN_REVIEW = "in review"
    VERIFIED = "verified"


FOOD_DOC_TYPE = "food"

