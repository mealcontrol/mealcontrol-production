from dataclasses import dataclass


@dataclass
class FoodFilterEntity:
    language: str = 'ru'
    is_validated: bool = True
