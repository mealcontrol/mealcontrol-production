from dataclasses import dataclass

from fastapi import HTTPException
from typing import List

from app.domain.entities import FoodBaseDBEntity
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IFoodDAO


@dataclass
class RequestObject:
    query_str: str
    skip: int = 0
    limit: int = 100


@dataclass
class ResponseObject:
    food: List[FoodBaseDBEntity]


class SearchFoodUseCase(IUseCase):
    Request = RequestObject

    def __init__(self, food_dao: IFoodDAO):
        self._food_dao = food_dao

    def execute(self, request_object: RequestObject) -> ResponseObject:
        food = self._food_dao.search(
            query_string=request_object.query_str,
            skip=request_object.skip,
            limit=request_object.limit
        )
        if not food:
            raise HTTPException(
                status_code=400,
                detail="The food with this name was not found.",
            )

        return ResponseObject(food=food)
