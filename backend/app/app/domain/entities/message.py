from pydantic import BaseModel


class MessageEntity(BaseModel):
    message: str
