from datetime import datetime

from pydantic import BaseModel

from app.domain.constants import FoodStateEnum


class FoodMetadataEntity(BaseModel):
    state: FoodStateEnum = FoodStateEnum.NEW
    updated_at: datetime = datetime.now()
    priority: int = 1
