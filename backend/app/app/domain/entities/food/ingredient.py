from typing import Optional

from pydantic import BaseModel


class IngredientEntity(BaseModel):
    name: str
    quantity: Optional[float] = None
