from pydantic import BaseModel
from typing import List, Optional

from .measure import MeasureEntity
from .metadata import FoodMetadataEntity
from .nutrient import NutrientEntity


class FoodBaseEntity(BaseModel):
    """
    Base class for main food properties
    """
    name: str
    language: str
    calories: int
    fats: float
    proteins: float
    carbs: float
    tags: List[str]
    owner_email: str = 'System'


class FoodCreateEntity(FoodBaseEntity):
    measures: List[MeasureEntity] = []
    nutrients: List[NutrientEntity] = []
    barcode: str = None


class FoodBaseDBEntity(FoodBaseEntity):
    id: str = None


class FoodBaseDBStateEntity(FoodBaseDBEntity):
    state: str


class FoodDetailEntity(FoodBaseDBEntity):
    """
    Detail view for food models.
    """
    measures: List[MeasureEntity] = []
    nutrients: List[NutrientEntity] = []
    metadata: FoodMetadataEntity = FoodMetadataEntity()
    barcode: str = None


class FoodInDBEntity(FoodDetailEntity):
    """
    Additional properties for storing in database
    """


class FoodUpdateEntity(FoodDetailEntity):
    """
    Update food entity
    """
    name: Optional[str] = None
    language: Optional[str] = None
    calories: Optional[int] = None
    fats: Optional[float] = None
    proteins: Optional[float] = None
    carbs: Optional[float] = None
    tags: Optional[List[str]] = []


class FoodSyncIn(FoodBaseEntity):
    """
    Properties for syncing in different buckets.
    """
    id: str
    name: str


class FoodIntakeEntity(BaseModel):
    id: str
    name: str
    calories: int
    fats: float
    proteins: float
    carbs: float

