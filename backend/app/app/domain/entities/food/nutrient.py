from pydantic import BaseModel


class NutrientEntity(BaseModel):
    name: str
    value: float
