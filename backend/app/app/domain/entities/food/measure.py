from pydantic import BaseModel


class MeasureEntity(BaseModel):
    measure_name: str
    value: float


class WaterMeasureEntity(BaseModel):
    name: str
    value: int
