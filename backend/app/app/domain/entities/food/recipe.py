from typing import Optional, List

from pydantic import BaseModel

from .ingredient import IngredientEntity


class RecipeBaseEntity(BaseModel):
    recipe_page: str = None


class RecipeInDBEntity(RecipeBaseEntity):
    ingredients: Optional[List[IngredientEntity]] = None


class RecipeRawEntity(RecipeInDBEntity):
    raw_ingredients: Optional[str] = None
