from typing import List

from pydantic import BaseModel

from app.domain.constants import RoleEnum


class Roles(BaseModel):
    roles: List[RoleEnum]
