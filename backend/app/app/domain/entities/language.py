from pydantic import BaseModel


class LanguageBaseEntity(BaseModel):
    language: str
    name: str
