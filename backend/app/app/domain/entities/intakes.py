from datetime import date

from pydantic import BaseModel
from typing import List

from app.domain.entities.food.food import FoodIntakeEntity


class ProductIntakeEntity(BaseModel):
    product: FoodIntakeEntity
    portion_size: float


class IntakeDetailEntity(BaseModel):
    intake_name: str
    products: List[ProductIntakeEntity] = []
    proteins: float = 0
    calories: int = 0
    carbs: float = 0
    fats: float = 0


class IntakeListEntity(BaseModel):
    date_: date
    owner_id: str
    intakes: List[IntakeDetailEntity]
    proteins: float = 0
    calories: int = 0
    carbs: float = 0
    fats: float = 0


class IntakeInDBEntity(IntakeListEntity):
    water_amount: int


class WaterIntakeEntity(BaseModel):
    date_: date
    owner_id: str
    water_amount: int
