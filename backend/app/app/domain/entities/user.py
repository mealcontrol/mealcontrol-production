from typing import Optional, Union, List

from pydantic import BaseModel, EmailStr

from app.domain.entities.food.measure import WaterMeasureEntity
from app.domain.entities.role import RoleEnum


class UserBase(BaseModel):
    email: Optional[EmailStr] = None
    role: Optional[Union[str, RoleEnum]] = None
    disabled: Optional[bool] = None


# Shared properties in Couchbase
class UserBaseInDB(UserBase):
    first_name: Optional[str] = None
    last_name: Optional[str] = None


# Properties to receive via API on creation
class UserCreate(UserBaseInDB):
    email: EmailStr
    password: str
    role: Union[str, RoleEnum] = RoleEnum.user.value
    disabled: bool = False


# Properties to receive via API on update
class UserUpdate(UserBaseInDB):
    password: Optional[str] = None


# Additional properties to return via API
class User(UserBaseInDB):
    id: str


# Additional properties stored in DB
class UserInDB(UserBaseInDB):
    id: str
    hashed_password: str
    email: EmailStr
    intakes: List[str] = []
    water_measures: List[WaterMeasureEntity] = []
    favourites: List[str] = []

