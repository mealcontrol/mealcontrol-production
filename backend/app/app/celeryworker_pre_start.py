import logging

from rethinkdb import RethinkDB
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app import config

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 5  # 5 minutes
wait_seconds = 1


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init():
    try:
        # Check Couchbase is awake
        r = RethinkDB()
        r.connect(
            host=config.DB_HOST,
            port=config.DB_PORT,
            db=config.DB_NAME
        )
        logger.info(
            f"Database connection established"
        )
    except Exception as e:
        logger.error(e)
        raise e


def main():
    logger.info("Initializing service")
    init()
    logger.info("Service finished initializing")


if __name__ == "__main__":
    main()
