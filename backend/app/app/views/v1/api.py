from fastapi import APIRouter

from app.views.v1.endpoints import login, roles, users, utils, food

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(roles.router, prefix="/roles", tags=["roles"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(food.router, prefix="/food", tags=["food"])
