from fastapi import APIRouter, Depends, HTTPException, Body
from typing import List

from app.containers import Services
from app.domain.authentication.helpers import is_superuser
from app.domain.authentication.user_auth import get_current_active_user
from app.views.v1.serializers.water_measures import WaterMeasureView

router = APIRouter()

user_settings_service = Services.get_user_settings_service()


@router.get('/', response_model=List[WaterMeasureView])
def get_water_measurements(user_id: str, current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to get this information!")
    return user_settings_service.get_water_measures(user_id)


@router.post('/', response_model=WaterMeasureView)
def create_water_measure(
        user_id: str,
        water_measure: WaterMeasureView = Body(..., alias='waterMeasure'),
        current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to create water measure for this user!")
    return user_settings_service.create_water_measure(user_id=user_id, measure=water_measure)


@router.get('/{measure_name}', response_model=WaterMeasureView)
def get_water_measure(
        user_id: str,
        measure_name: str,
        current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to get water measure of this user!")
    return user_settings_service.get_water_measure(user_id=user_id, measure_name=measure_name)


@router.put('/{measure_name}', response_model=WaterMeasureView)
def update_water_measure(
        user_id: str,
        measure_name: str,
        water_measure: WaterMeasureView = Body(..., alias='waterMeasure'),
        current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to update water measure of this user!")
    return user_settings_service.update_water_measure(user_id=user_id, measure_name=measure_name, measure=water_measure)


@router.delete('/{measure_name}', response_model=bool)
def delete_water_measure(
        user_id: str,
        measure_name: str,
        current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to get water measure of this user!")
    return user_settings_service.delete_water_measure(user_id=user_id, measure_name=measure_name)