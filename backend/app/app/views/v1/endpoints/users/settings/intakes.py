from fastapi import APIRouter, Depends, HTTPException, Body
from typing import List

from app.containers import Services
from app.domain.authentication.helpers import is_superuser
from app.domain.authentication.user_auth import get_current_active_user

router = APIRouter()

user_settings_service = Services.get_user_settings_service()


@router.get('/', response_model=List[str])
def get_user_intakes(user_id: str, current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to get this information!")
    return user_settings_service.get_intakes(user_id)


@router.put('/', response_model=List[str])
def update_user_intakes(user_id: str, intakes: List[str], current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to update this information!")
    return user_settings_service.update_intakes(user_id, intakes)


@router.post('/', response_model=List[str])
def add_user_intake(user_id: str,
                    intake: str = Body(..., alias='intake'),
                    current_user: get_current_active_user = Depends()):
    if not is_superuser(current_user) and user_id != current_user.id:
        raise HTTPException(status_code=400, detail="You are not permitted to create new intake!")
    return user_settings_service.create_intake(user_id, intake)
