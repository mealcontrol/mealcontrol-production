from fastapi import APIRouter
from .intakes import router as user_intakes_router
from .water_measures import router as user_water_settings_router

settings_router = APIRouter()
settings_router.include_router(user_intakes_router, prefix='/intakes')
settings_router.include_router(user_water_settings_router, prefix='/water')
