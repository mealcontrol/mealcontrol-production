from fastapi import APIRouter
from .intakes import router as settings_intakes_router
from .water_measures import router as settings_water_settings_router

router = APIRouter()
router.include_router(settings_intakes_router, prefix='/intakes')
router.include_router(settings_water_settings_router, prefix='/water')
