from datetime import datetime

from fastapi import APIRouter, Depends, HTTPException, Body

from app.containers import Services
from app.domain.authentication.helpers import is_superuser
from app.domain.authentication.user_auth import get_current_active_user
from app.domain.entities import UserInDB
from app.views.v1.serializers.intake import WaterIntakeView

router = APIRouter()

water_service = Services.get_water_service()


@router.get('/{date_}', response_model=WaterIntakeView)
def get_daily_water(user_id: str, date_: str, current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_object = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    if not is_superuser(current_user) and current_user.id != user_id:
        raise HTTPException(status_code=400, detail="You are not permitted to get this information")

    daily_water = water_service.get_daily_water(user_id=user_id, water_date=date_object)
    return WaterIntakeView.from_entity(daily_water)


@router.post('/{date_}', response_model=int)
def add_water(user_id: str,
              date_: str,
              water_amount: int = Body(..., alias='waterAmount', gt=0),
              current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_object = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    if not is_superuser(current_user) and current_user.id != user_id:
        raise HTTPException(status_code=400, detail="You are not permitted to get this information")

    daily_water = water_service.add_water(user_id=user_id, water_date=date_object, water_amount=water_amount)
    return daily_water


@router.delete('/{date_}', response_model=bool)
def remove_water(user_id: str,
                 date_: str,
                 water_amount: int = Body(..., alias='waterAmount', gt=0),
                 current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_object = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    if not is_superuser(current_user) and current_user.id != user_id:
        raise HTTPException(status_code=400, detail="You are not permitted to get this information")

    result = water_service.remove_water(user_id=user_id, water_date=date_object, water_amount=water_amount)
    return result
