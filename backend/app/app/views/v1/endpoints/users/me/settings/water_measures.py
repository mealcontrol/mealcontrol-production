from typing import List

from fastapi import APIRouter, Depends, Body

from app.containers import Services
from app.domain.authentication.user_auth import get_current_active_user
from app.views.v1.serializers.water_measures import WaterMeasureView

router = APIRouter()

user_settings_service = Services.get_user_settings_service()


@router.get('/', response_model=List[WaterMeasureView])
def get_water_measurements_me(current_user: get_current_active_user = Depends()):
    return user_settings_service.get_water_measures(current_user.id)


@router.post('/', response_model=WaterMeasureView)
def create_water_measure_me(
        water_measure: WaterMeasureView = Body(..., alias='waterMeasure'),
        current_user: get_current_active_user = Depends()):
    return user_settings_service.create_water_measure(user_id=current_user.id, measure=water_measure)


@router.get('/{measure_name}', response_model=WaterMeasureView)
def get_water_measure_me(
        measure_name: str,
        current_user: get_current_active_user = Depends()):
    return user_settings_service.get_water_measure(user_id=current_user.id, measure_name=measure_name)


@router.put('/{measure_name}', response_model=WaterMeasureView)
def update_water_measure_me(
        measure_name: str,
        water_measure: WaterMeasureView = Body(..., alias='waterMeasure'),
        current_user: get_current_active_user = Depends()):
    return user_settings_service.update_water_measure(user_id=current_user.id,
                                                      measure_name=measure_name, measure=water_measure)


@router.delete('/{measure_name}', response_model=bool)
def delete_water_measure_me(
        measure_name: str,
        current_user: get_current_active_user = Depends()):
    return user_settings_service.delete_water_measure(user_id=current_user.id, measure_name=measure_name)
