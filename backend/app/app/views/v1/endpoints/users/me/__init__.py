from .intakes import router as me_intakes_router
from .water import router as me_water_router
from .settings import router as me_settings_router
from .favourites import router as me_user_favourites_router
