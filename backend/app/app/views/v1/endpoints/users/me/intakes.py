from datetime import datetime
from typing import List

from fastapi import APIRouter, HTTPException, Depends, Query, Body

from app.containers import Services
from app.domain.authentication.user_auth import get_current_active_user
from app.domain.entities import UserInDB
from app.views.v1.serializers.intake import IntakeListView

router = APIRouter()

intake_service = Services.get_intake_service()


@router.get('/{date_}', response_model=IntakeListView)
def get_daily_intakes_me(date_: str, current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_object = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    daily_intakes = intake_service.get_intake_by_day(user_id=current_user.id, intake_date=date_object)
    return IntakeListView.from_entity(daily_intakes)


@router.get('/', response_model=List[IntakeListView])
def get_intakes_list_by_date_range_me(date_start_from: str = Query(..., alias='dateStartFrom'),
                                      date_end_to: str = Query(..., alias='dateEndTo'),
                                      current_user: UserInDB = Depends(get_current_active_user)
                                      ):
    try:
        date_start_from = datetime.strptime(date_start_from, '%d-%m-%Y').date()
        date_end_to = datetime.strptime(date_end_to, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    daily_intakes_list = intake_service.get_intakes_by_date_range(
        user_id=current_user.id, intake_date_start=date_start_from, intake_date_end=date_end_to)

    return [IntakeListView.from_entity(intakes) for intakes in daily_intakes_list]


@router.post('/{date_}/products', response_model=IntakeListView)
def add_product_to_intake_me(date_: str,
                             food_id: str = Body(..., alias='foodId'),
                             portion_size: int = Body(..., alias='portionSize'),
                             intake_name: str = Body(..., alias='intakeName'),
                             current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_ = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    result = intake_service.add_product_to_intake(
        user_id=current_user.id, intake_date=date_, intake_name=intake_name, food_id=food_id, portion_size=portion_size)
    return IntakeListView.from_entity(result)


@router.put('/{date_}/products', response_model=IntakeListView)
def update_product_in_intake_me(date_: str,
                                food_id: str = Body(..., alias='foodId'),
                                portion_size: int = Body(..., alias='portionSize'),
                                intake_name: str = Body(..., alias='intakeName'),
                                current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_ = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    result = intake_service.update_product_in_intake(
        user_id=current_user.id, intake_date=date_, intake_name=intake_name, food_id=food_id, portion_size=portion_size)
    return IntakeListView.from_entity(result)


@router.delete('/{date_}/products', response_model=IntakeListView)
def delete_product_from_intake_me(date_: str,
                                  food_id: str = Body(..., alias='foodId'),
                                  intake_name: str = Body(..., alias='intakeName'),
                                  current_user: UserInDB = Depends(get_current_active_user)):
    try:
        date_ = datetime.strptime(date_, '%d-%m-%Y').date()
    except ValueError:
        raise HTTPException(status_code=400, detail="Wrong date format, try to use dd-mm-YYYY")

    result = intake_service.remove_product_from_intake(
        user_id=current_user.id, intake_date=date_, intake_name=intake_name, food_id=food_id)
    return IntakeListView.from_entity(result)
