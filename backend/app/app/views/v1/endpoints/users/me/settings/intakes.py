from typing import List

from fastapi import APIRouter, Depends, Body

from app.containers import Services
from app.domain.authentication.user_auth import get_current_active_user

router = APIRouter()

user_settings_service = Services.get_user_settings_service()


@router.get('/', response_model=List[str])
def get_user_intakes_me(current_user: get_current_active_user = Depends()):
    print(current_user)
    return user_settings_service.get_intakes(current_user.id)


@router.put('/', response_model=List[str])
def update_user_intakes_me(intakes: List[str], current_user: get_current_active_user = Depends()):
    return user_settings_service.update_intakes(current_user.id, intakes)


@router.post('/', response_model=List[str])
def add_user_intake_me(intake: str = Body(..., alias='intake'),
                       current_user: get_current_active_user = Depends()):
    return user_settings_service.create_intake(current_user.id, intake)
