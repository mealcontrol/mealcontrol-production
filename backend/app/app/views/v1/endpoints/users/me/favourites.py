from typing import List

from fastapi import APIRouter, Depends, HTTPException, Body

from app.containers import Services
from app.domain.authentication.helpers import is_superuser
from app.domain.authentication.user_auth import get_current_active_user
from app.domain.entities import UserInDB
from app.views.v1.serializers.food import FoodBaseStateView, FoodDetailView

router = APIRouter()

user_favourite_service = Services.get_user_favourites_service()


@router.get('/', response_model=List[FoodBaseStateView])
def get_user_favourites(
        current_user: UserInDB = Depends(get_current_active_user),
):

    favourite_food = user_favourite_service.get_user_favourites(user_id=current_user.id)
    return [FoodBaseStateView.from_entity(food) for food in favourite_food]


@router.post('/', response_model=FoodDetailView)
def add_user_favourite(
        food_id: str = Body(..., alias='foodId'),
        current_user: UserInDB = Depends(get_current_active_user)
):

    favourite_food = user_favourite_service.create_user_favourite(user_id=current_user.id, food_id=food_id)
    return [FoodDetailView.from_entity(food) for food in favourite_food]


@router.delete('/{food_id}', response_model=bool)
def remove_user_favourite(
        food_id: str,
        current_user: UserInDB = Depends(get_current_active_user)
):

    result = user_favourite_service.remove_user_favourite(user_id=current_user.id, food_id=food_id)
    return result
