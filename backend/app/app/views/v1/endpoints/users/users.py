from typing import List

from fastapi import APIRouter, Body, Depends, HTTPException
from pydantic.types import EmailStr

from app.containers import Services
from app.domain.authentication.user_auth import get_current_active_superuser, get_current_active_user
from app.domain.entities.user import UserCreate, UserInDB, UserUpdate
from app.views.v1.serializers.users import UserView, UserUpdateView, UserCreateView

router = APIRouter()

user_service = Services.get_user_service()


@router.get("/", response_model=List[UserView])
def get_users(
        skip: int = 0,
        limit: int = 100,
        current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Retrieve users.
    """
    users = user_service.get_list(current_user=current_user, skip=skip, limit=limit)

    return [UserView(
        **{'firstName': user.first_name, 'lastName': user.last_name},
        **user.dict()
    ).dict(by_alias=True) for user in users]


@router.get("/search/", response_model=List[UserView])
def search_users(
        q: str,
        skip: int = 0,
        limit: int = 100,
        current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Search users, use Bleve Query String syntax:
    http://blevesearch.com/docs/Query-String-Query/

    For typeahead suffix with `*`. For example, a query with: `email:johnd*` will match
    users with email `johndoe@example.com`, `johndid@example.net`, etc.
    """
    users = user_service.search(query_string=q, skip=skip, limit=limit)
    return [UserView(
        **{'firstName': user.first_name, 'lastName': user.last_name},
        **user.dict()
    ).dict(by_alias=True) for user in users]


@router.post("/", response_model=UserView)
def create_user(
        *,
        user_in: UserCreateView,
        current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Create new user.
    Available only for superuser
    """
    created_user = user_service.create(user_in=UserCreate(**user_in.dict(by_alias=False)))
    return UserView(
        **{'firstName': created_user.first_name, 'lastName': created_user.last_name},
        **created_user.dict()
    ).dict(by_alias=True)


@router.put("/me", response_model=UserView)
def update_user_me(
        *,
        user_to_update: UserUpdateView = Body(...),
        current_user: UserInDB = Depends(get_current_active_user),
):
    """
    Update own user.
    """
    updated_user = user_service.update(
        user_id=current_user.id,
        user_to_update=UserUpdate(**user_to_update.dict(by_alias=False, skip_defaults=True)),
        current_user=current_user
    )
    return UserView(
        **{'firstName': updated_user.first_name, 'lastName': updated_user.last_name},
        **updated_user.dict()
    ).dict(by_alias=True)


@router.get("/me", response_model=UserView)
def read_user_me(current_user: UserInDB = Depends(get_current_active_user)):
    """
    Get current user.
    """
    return UserView(
        **{'firstName': current_user.first_name, 'lastName': current_user.last_name},
        **current_user.dict()
    ).dict(by_alias=True)


@router.post("/register", response_model=UserView)
def create_user_open(
        *,
        email: EmailStr = Body(...),
        password: str = Body(...),
        first_name: str = Body(..., alias='firstName'),
        last_name: str = Body(..., alias='lastName'),
):
    """
    Create new user without the need to be logged in.
    """
    new_user = user_service.register(email=email, password=password, first_name=first_name, last_name=last_name)
    return UserView(
        **{'firstName': new_user.first_name, 'lastName': new_user.last_name},
        **new_user.dict()
    ).dict(by_alias=True)


@router.get("/{user_id}", response_model=UserView)
def read_user(user_id: str, current_user: UserInDB = Depends(get_current_active_user)):
    """
    Get a specific user by user_id.
    """
    user_info = user_service.get_detail(user_id=user_id, current_user=current_user)
    if not user_info:
        raise HTTPException(
            status_code=404,
            detail=f"User with id {user_id} was not found"
        )
    return UserView(
        **{'firstName': user_info.first_name, 'lastName': user_info.last_name},
        **user_info.dict()
    ).dict(by_alias=True)


@router.put("/{user_id}", response_model=UserView)
def update_user(
        *,
        user_id: str,
        user_in: UserUpdateView,
        current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Update a user.
    """
    updated_user = user_service.update(
        user_id=user_id,
        user_to_update=UserUpdate(**user_in.dict(by_alias=False, skip_defaults=True)),
        current_user=current_user
    )

    return UserView(
        **{'firstName': updated_user.first_name, 'lastName': updated_user.last_name},
        **updated_user.dict()
    ).dict(by_alias=True)

