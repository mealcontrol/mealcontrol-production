from .users import router
from .favourites import router as user_favourites_router
from .settings import settings_router
from .intakes import router as intakes_router
from .water import router as water_router
from .me import (
    me_intakes_router,
    me_settings_router,
    me_water_router,
    me_user_favourites_router,
)

router.include_router(me_intakes_router, prefix='/me/intakes', tags=['Intakes', 'Me'])
router.include_router(me_settings_router, prefix='/me/settings', tags=['Settings', 'Me'])
router.include_router(me_water_router, prefix='/me/water', tags=['Water', 'Me'])
router.include_router(me_user_favourites_router, prefix='/me/favourites', tags=['Favourites', 'Me'])
router.include_router(settings_router, prefix='/{user_id}/settings', tags=['Settings'])
router.include_router(intakes_router, prefix='/{user_id}/intakes', tags=['Intakes'])
router.include_router(water_router, prefix='/{user_id}/water', tags=['Water'])
router.include_router(user_favourites_router, prefix='/{user_id}/favourites', tags=['Favourites'])
