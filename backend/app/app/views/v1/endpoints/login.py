from fastapi import APIRouter, Body, Depends
from fastapi.security import OAuth2PasswordRequestForm

from app.containers import use_cases
from app.domain.authentication.user_auth import get_current_active_user
from app.domain.entities import TokenEntity
from app.domain.entities.message import MessageEntity
from app.domain.entities.user import UserInDB
from app.views.v1.serializers.users import UserView

router = APIRouter()


@router.post("/login/access-token", response_model=TokenEntity)
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    OAuth2 compatible token login, get an access token for future requests.
    """
    login_use_case = use_cases.login()
    token = login_use_case.execute(email=form_data.username, password=form_data.password)
    return token


@router.post("/login/test-token", response_model=UserView)
def test_token(current_user: UserInDB = Depends(get_current_active_user)):
    """
    Test access token.
    """
    return dict(
        **{'firstName': current_user.first_name, 'lastName': current_user.last_name},
        **current_user.dict()
    )


@router.post("/password-recovery/{email}", response_model=MessageEntity)
def recover_password(email: str):
    """
    Password Recovery.
    """
    recover_password_use_case = use_cases.recover_password()
    recover_password_use_case.execute(email=email)
    return {"msg": "Password recovery email sent"}


@router.post("/reset-password/", response_model=MessageEntity)
def reset_password(
        token: str = Body(...),
        new_password: str = Body(..., alias='newPassword')):
    """
    Reset password.
    """
    reset_password_use_case = use_cases.reset_password()
    reset_password_use_case.execute(token=token, new_password=new_password)
    return {"msg": "Password updated successfully"}
