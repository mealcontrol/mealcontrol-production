from dataclasses import asdict
from typing import List, Optional

from fastapi import APIRouter, Depends

from app.containers import use_cases, Services
from app.domain.authentication.user_auth import get_current_active_user, get_current_active_superuser
from app.domain.dtos import FoodFilterEntity
from app.domain.entities import FoodDetailEntity, FoodCreateEntity
from app.domain.entities.food.food import FoodUpdateEntity
from app.domain.entities.user import UserInDB
from app.views.v1.serializers.food import FoodCreateView, FoodUpdateView, FoodBaseView, FoodDetailView, \
    FoodBaseStateView

router = APIRouter()
food_service = Services.get_food_service()


@router.get("/", response_model=List[FoodBaseStateView])
def get_all_food(
        *,
        language: Optional[str] = 'ru',
        valid: Optional[bool] = None,
        skip: int = 0,
        limit: int = 100,
        current_user: UserInDB = Depends(get_current_active_user),
):
    """
    Retrieve food.
    """
    food_list = food_service.get_list(
        user=current_user,
        filter_=FoodFilterEntity(language=language, is_validated=valid),
        skip=skip,
        limit=limit
    )
    return [FoodBaseStateView.from_entity(food) for food in food_list]


@router.get("/search/", response_model=List[FoodBaseView])
def search_food(
    q: str,
    skip: int = 0,
    limit: int = 100,
    current_user: UserInDB = Depends(get_current_active_user),
):
    """
    Search items, use Bleve Query String syntax:
    http://blevesearch.com/docs/Query-String-Query/

    For typeahead suffix with `*`. For example, a query with: `title:foo*` will match
    items containing `football`, `fool proof`, etc.
    """
    # TODO: fix food search in the future.
    search_food_use_case = use_cases.search_food()
    food_request = search_food_use_case.Request(
        query_str=q,
        skip=skip,
        limit=limit
    )
    food_list = search_food_use_case.execute(food_request)
    result = [FoodBaseView(**asdict(food)) for food in food_list]
    return result


@router.post("/", response_model=FoodDetailView)
def create_food(
    *,
    food_in: FoodCreateView,
    current_user: UserInDB = Depends(get_current_active_user)
):
    """
    Create new food.
    """
    created_food = food_service.create(
        food_in=FoodCreateEntity(**food_in.dict(by_alias=False)),
        user=current_user
    )
    return FoodDetailView.from_entity(created_food)


@router.put("/{id}", response_model=FoodDetailView)
def update_food(
    id: str,
    *,
    food_in: FoodUpdateView,
    current_user: UserInDB = Depends(get_current_active_user),
):
    """
    Update food.
    """
    updated_food = food_service.update(
        FoodUpdateEntity(id=id, **food_in.dict(by_alias=False, exclude={'id'})),
        user=current_user
    )
    return FoodDetailView.from_entity(updated_food)


@router.get("/{id}", response_model=FoodDetailView)
def get_food_detail(
        id: str,
        current_user: UserInDB = Depends(get_current_active_user)
):
    """
    Get food by ID.
    """
    food_detail = food_service.get_detail(food_id=id, user=current_user)
    return FoodDetailView.from_entity(food_detail)


@router.delete("/{id}", response_model=bool)
def delete_food(
        id: str,
        current_user: UserInDB = Depends(get_current_active_user)
):
    """
    Delete a food by ID.
    """
    result = food_service.delete(food_id=id, user=current_user)
    return result


@router.put("/{id}/validate", response_model=FoodDetailView)
def validate_food(
        id: str,
        *,
        food_in: FoodUpdateView,
        current_user: UserInDB = Depends(get_current_active_superuser)
):
    """
    Validating of food entity.
    """
    validated_food = food_service.validate(
        food_in=FoodDetailEntity(id=id, **food_in.dict(by_alias=False, exclude={'id'})),
        user=current_user
    )
    return FoodDetailView.from_entity(validated_food)

