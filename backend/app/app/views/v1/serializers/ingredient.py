from pydantic import BaseModel, Schema


class IngredientView(BaseModel):
    name: str = Schema(..., title="Name of ingredient")
    quantity: float = Schema(None, title="Quantity of product, in g")
