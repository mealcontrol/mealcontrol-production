from uuid import UUID

from pydantic import EmailStr, Schema

from app.domain.constants import RoleEnum
from app.domain.entities import User, UserUpdate, UserCreate


class UserView(User):
    id: UUID = Schema(...)
    email: EmailStr = Schema(...)
    first_name: str = Schema(default=None, alias="firstName")
    last_name: str = Schema(default=None, alias="lastName")
    disabled: bool = Schema(default=False)
    role: RoleEnum = Schema(..., description="User privilege")


class UserUpdateView(UserUpdate):
    first_name: str = Schema(default=None, alias='firstName')
    last_name: str = Schema(default=None, alias='lastName')


class UserCreateView(UserCreate):
    first_name: str = Schema(default=None, alias='firstName')
    last_name: str = Schema(default=None, alias='lastName')
