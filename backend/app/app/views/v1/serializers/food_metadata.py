from datetime import datetime

from pydantic import Schema, validator

from app.domain.constants import FoodStateEnum
from app.domain.entities import FoodMetadataEntity


class FoodMetaView(FoodMetadataEntity):
    state: FoodStateEnum
    updated_at: datetime = Schema(datetime.now(), alias="updatedAt")
    priority: int = 1

    @validator('updated_at')
    def update_time(cls, _):
        return datetime.now()

    @classmethod
    def from_entity(cls, entity: FoodMetadataEntity):
        return dict(
            state=entity.state,
            priority=entity.priority,
            **{'updatedAt': entity.updated_at},
        )
