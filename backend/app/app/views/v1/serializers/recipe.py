from typing import Optional, List

from pydantic import Schema

from app.domain.entities import RecipeBaseEntity
from .ingredient import IngredientView


class RecipeBaseView(RecipeBaseEntity):
    recipe_page: str = Schema(None, title="HTML page that include recipe of food",
                              alias="recipePage")


class RecipeView(RecipeBaseView):
    ingredients: Optional[List[IngredientView]] = Schema(None, title="List of ingredients")


class RecipeRawView(RecipeView):
    raw_ingredients: Optional[str] = Schema(None, title="String, includes ingredients of food",
                                            alias="rawIngredients")
