from pydantic import Schema

from app.domain.entities.food.measure import WaterMeasureEntity


class WaterMeasureView(WaterMeasureEntity):
    name: str = Schema(..., title="Name of water measure of volume")
    value: int = Schema(..., title="Water measurement", description="Size of water measurement in ml")
