from datetime import date

from pydantic import Schema
from typing import List

from app.domain.entities.intakes import IntakeListEntity, IntakeDetailEntity, ProductIntakeEntity, WaterIntakeEntity


class ProductView(ProductIntakeEntity):
    portion_size: int = Schema(..., alias='portionSize')

    @classmethod
    def from_entity(cls, entity: ProductIntakeEntity):
        return dict(
            **entity.dict(exclude={'portion_size'}),
            portionSize=entity.portion_size
        )


class IntakeView(IntakeDetailEntity):
    intake_name: str = Schema(..., alias='intakeName')
    products: List[ProductView]

    @classmethod
    def from_entity(cls, entity: IntakeDetailEntity):
        return dict(
            **entity.dict(exclude={'products', 'intake_name'}),
            intakeName=entity.intake_name,
            products=[ProductView.from_entity(product) for product in entity.products]
        )


class IntakeListView(IntakeListEntity):
    date_: date = Schema(..., alias='date')
    owner_id: str = Schema(..., alias='userId')
    intakes: List[IntakeView]

    @classmethod
    def from_entity(cls, entity: IntakeListEntity):
        return dict(
            **entity.dict(exclude={'intakes', 'date_', 'owner_id'}),
            date=entity.date_,
            intakes=[IntakeView.from_entity(intake) for intake in entity.intakes],
            userId=entity.owner_id
        )


class WaterIntakeView(WaterIntakeEntity):
    date_: date = Schema(..., alias='date')
    owner_id: str = Schema(..., alias='userId')
    water_amount: int = Schema(..., alias='waterAmount')

    @classmethod
    def from_entity(cls, entity: WaterIntakeEntity):
        return dict(
            date=entity.date_,
            userId=entity.owner_id,
            waterAmount=entity.water_amount
        )
