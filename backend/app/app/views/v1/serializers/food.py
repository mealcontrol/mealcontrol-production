from pydantic import Schema
from typing import List, Optional

from .food_metadata import FoodMetaView
from .measure import Measure
from .nutrient import Nutrient
from app.domain.entities import FoodBaseEntity, FoodDetailEntity


class FoodBase(FoodBaseEntity):
    """
    View class for fields
    """
    name: str = Schema(..., description="Name of product")
    language: str = Schema("ru", description="Language of product")
    calories: int = Schema(0, ge=0, description="Number of calories in 100g(ml) of product")
    fats: float = Schema(0, ge=0, description="Number of fats in 100g(ml) of product")
    proteins: float = Schema(0, ge=0, description="Number of proteins in 100g(ml) of product")
    carbs: float = Schema(0, ge=0, description="Number of carbs in 100g(ml) of product")
    tags: List[str] = Schema([], title="Tags or categories of food.")
    owner_email: str = Schema("System", alias="ownerEmail")


class FoodBaseView(FoodBase):
    id: str

    @classmethod
    def from_entity(cls, entity: FoodDetailEntity):
        return dict(
            **entity.dict(exclude={'measures'}),
            **{'ownerEmail': entity.owner_email}
        )


class FoodBaseStateView(FoodBaseView):
    state: str

    @classmethod
    def from_entity(cls, entity: FoodDetailEntity):
        return dict(
            **entity.dict(exclude={'measures'}),
            **{'ownerEmail': entity.owner_email, 'state': entity.metadata.state.value}
        )


class FoodDetailView(FoodBaseView, FoodDetailEntity):
    measures: List[Measure] = Schema([], description="List of product measures")
    nutrients: List[Nutrient] = Schema([], title="List of nutrients", alias="nutrients")
    metadata: FoodMetaView
    owner_email: str = Schema("System", alias="ownerEmail")
    barcode: Optional[str] = Schema(None, alias='barcode')

    @classmethod
    def from_entity(cls, entity: FoodDetailEntity):
        return dict(
            **entity.dict(exclude={'measures', 'metadata'}),
            measures=[Measure.from_entity(measure_entity) for measure_entity in entity.measures],
            **{'ownerEmail': entity.owner_email},
            metadata=FoodMetaView.from_entity(entity.metadata)
        )


class FoodCreateView(FoodBase):
    """
    Fields for in creating new food.
    """
    measures: List[Measure] = Schema([], description="List of product measures")
    nutrients: List[Nutrient] = Schema([], title="List of nutrients", alias="nutrients")
    barcode: Optional[str] = Schema(None, alias='barcode')


class FoodUpdateView(FoodDetailView):
    """
    Fields required for updating fields
    """
    metadata: Optional[FoodMetaView] = None
