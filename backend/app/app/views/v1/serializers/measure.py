from pydantic import Schema
from typing import Dict

from app.domain.entities.food.measure import MeasureEntity


class Measure(MeasureEntity):
    value: float = Schema(..., gt=0)
    measure_name: str = Schema(..., alias="measureName", description="Name of measure, e.g. spoon")

    @classmethod
    def from_entity(cls, entity: MeasureEntity):
        return dict(**entity.dict(), **{'measureName': entity.measure_name})
