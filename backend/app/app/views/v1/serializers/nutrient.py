from pydantic import BaseModel, Schema


class Nutrient(BaseModel):
    name: str = Schema(..., title="Name of nutrient.")
    value: float = Schema(..., title="Value of nutrient")
