from datetime import datetime
from typing import List, Optional

from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from rethinkdb import RethinkDB

from app.config import FOOD_OWNER_SYSTEM
from app.domain.constants import FoodStateEnum
from app.domain.dtos import FoodFilterEntity
from app.domain.entities import FoodDetailEntity, FoodBaseDBEntity, FoodCreateEntity
from app.domain.entities.food.food import FoodUpdateEntity
from app.domain.interfaces.daos import IFoodDAO
from app.infrastructure.db_connection import DBConnection
from app.infrastructure.utils import generate_new_id


class FoodDAO(IFoodDAO):

    def __init__(self):
        self.r = RethinkDB()
        self.table = "food"
        self.connection = DBConnection(self.r)

    def create(self, *, food_in: FoodCreateEntity, username: str) -> FoodDetailEntity:
        existed_products = self.get_by_name(food_in.name)
        if existed_products:
            raise HTTPException(status_code=400, detail=f"Product with name {food_in.name} already exists")
        food_id = generate_new_id()
        food_in = FoodDetailEntity(
            **food_in.dict(skip_defaults=True, exclude={'owner_email'}), owner_email=username, id=food_id
        )
        with self.connection as c:
            result = self.r.table(self.table).insert(jsonable_encoder(food_in)).run(c)
        print(result)
        if result.get('inserted') == 1:
            return food_in
        raise HTTPException(status_code=400, detail=f"Product with name {food_in.name} wasn't saved")

    def get(self, food_id: str) -> FoodDetailEntity:
        with self.connection as c:
            food = self.r.table(self.table).get(food_id).run(c)
        return FoodDetailEntity(**food)

    def get_by_name(self, name: str) -> Optional[FoodDetailEntity]:
        with self.connection as c:
            results = self.r.table(self.table).filter(dict(name=name)).run(c)
        if len(results.items) > 0:
            return FoodDetailEntity(**results.items[0])
        return None

    def get_list(self, *, user_email: str, skip=0,
                 limit=100, food_filter: FoodFilterEntity) -> List[FoodDetailEntity]:

        with self.connection as c:
            # TODO: Find the way to rework this
            if food_filter.is_validated:
                result = self.r.table(self.table).filter(
                   (
                       (
                           (self.r.row['owner_email'] == user_email)
                           | (
                                (self.r.row['owner_email'] == FOOD_OWNER_SYSTEM)
                                & (self.r.row['metadata']['state'] == FoodStateEnum.VERIFIED.value)
                           )
                       )
                       & (self.r.row['language'] == food_filter.language)
                   )
                ).skip(skip).limit(limit).run(c)
            else:
                result = self.r.table(self.table).filter(
                    (
                        (
                            (self.r.row['owner_email'] == user_email)
                            | (self.r.row['owner_email'] == FOOD_OWNER_SYSTEM)
                        )
                        & (self.r.row['language'] == food_filter.language)
                    )
                ).skip(skip).limit(limit).run(c)

        return [FoodDetailEntity(**food) for food in result]

    def search(self, *, query_string: str, skip=0, limit=100) -> List[FoodBaseDBEntity]:
        return []

    def update(self, *, food_update: FoodUpdateEntity, state: FoodStateEnum = None) -> FoodDetailEntity:
        existed_food = self.get(food_update.id)

        if not existed_food:
            raise HTTPException(status_code=400, detail=f"Food with name {food_update.name} doesn't exist")

        entity = food_update.dict(skip_defaults=False, exclude={'id'})
        if state:
            if entity['metadata']:
                entity['metadata']['state'] = state.value
            else:
                entity['metadata'] = dict(state=state.value)

        entity = jsonable_encoder(entity)
        current_time = datetime.now()
        entity['metadata']['updated_at'] = self.r.time(
            current_time.year, current_time.month, current_time.day, 'Z'
        )
        with self.connection as c:
            result = self.r.table(self.table).get(food_update.id).update(entity, return_changes=True).run(c)
        changes = result.get('changes')
        if changes:
            new_entity = changes[0].get('new_val')
            if new_entity:
                return FoodDetailEntity(**new_entity)
        raise HTTPException(status_code=400, detail=f"Food with id {food_update.id} wasn't updated")

    def delete(self, food_id: str) -> bool:
        with self.connection as c:
            result = self.r.table(self.table).get(food_id).delete().run(c)
        return result.get('deleted', 0) > 0
