from typing import List

from fastapi import HTTPException
from rethinkdb import RethinkDB

from app.domain.entities import FoodDetailEntity, UserInDB
from app.domain.interfaces.daos import IUserFavouritesDAO
from app.infrastructure.db_connection import DBConnection


class UserFavouritesDAO(IUserFavouritesDAO):

    def __init__(self):
        self.r = RethinkDB()
        self.table = "users"
        self.food_table = "food"
        self.connection = DBConnection(self.r)

    def get_user_favourites(self, *, user_id: str) -> List[FoodDetailEntity]:
        with self.connection as c:
            user = self.r.table(self.table).get(user_id).run(c)
        if user:
            user_from_db = UserInDB(**user)

            food_favourites = user_from_db.favourites
            food_list = self.r.table(self.food_table).get_all(food_favourites).run(c)
            if food_list:
                return [FoodDetailEntity(**food) for food in food_list]
        return []

    def add_user_favourite(self, *, user_id: str, food_id: str) -> FoodDetailEntity:
        with self.connection as c:
            user = self.r.table(self.table).get(user_id).run(c)

            if not user:
                raise HTTPException(status_code=400, detail='User not found')

            user_from_db = UserInDB(**user)

            if food_id in user_from_db.favourites:
                raise HTTPException(status_code=400, detail='Food was already added to favourites')

            food = self.r.table(self.food_table).get(food_id).run(c)

            if not food:
                raise HTTPException(status_code=400, detail=f"Food with id {food_id} wasn't found")

            food_favourites = user_from_db.favourites.append(food_id)
            response = self.r.table(self.table).get(user_id).update({"favourites": food_favourites})
            if response:
                return food
        raise HTTPException(status_code=400, detail="Food wasn't added")

    def remove_user_favourite(self, *, user_id: str, food_id: str) -> bool:
        with self.connection as c:
            user = self.r.table(self.table).get(user_id).run(c)

            if not user:
                raise HTTPException(status_code=400, detail='User not found')

            user_from_db = UserInDB(**user)

            if food_id not in user_from_db.favourites:
                raise HTTPException(status_code=400, detail='Food was already removed from favourites')

            user_from_db.favourites.remove(food_id)
            response = self.r.table(self.table).get(user_id).update({"favourites": user_from_db.favourites})
            if response:
                return True
        return False
