from datetime import date

from fastapi import HTTPException
from rethinkdb import RethinkDB

from app.domain.entities.food.measure import WaterMeasureEntity
from app.domain.entities.intakes import WaterIntakeEntity
from app.domain.interfaces.daos import IWaterDAO
from app.infrastructure.db_connection import DBConnection


class WaterDAO(IWaterDAO):
    def __init__(self):
        self.r = RethinkDB()
        self.connection = DBConnection(self.r)
        self.table = "intakes"

    def add_water(self, *, user_id: str, water_date: date, water_amount: int) -> int:
        document_id = self.get_doc_id(user_id, str(water_date))
        with self.connection as c:
            result = self.r.table(self.table).get(document_id).update(
                dict(water_amount=self.r.row['water_amount'] + water_amount),
                return_changes=True
            )
        changes = result.get('changes')
        if changes:
            new_entity = changes[0].get('new_val')
            if new_entity:
                return water_amount
        raise HTTPException(status_code=400, detail=f'Water has not been added')

    def remove_water(self, *, user_id: str, water_date: date, water_amount) -> bool:
        document_id = self.get_doc_id(user_id, str(water_date))
        with self.connection as c:
            result = self.r.table(self.table).get(document_id).update(
                lambda intake: self.r.branch(
                    intake['water_amount'] - water_amount >= 0,
                    {'water_amount': intake['water_amount'] - water_amount},
                    {'water_amount': intake['water_amount']}
                ),
                return_changes=True
            ).run(c)
        changes = result.get('changes')
        if changes:
            new_entity = changes[0].get('new_val')
            old_entity = changes[0].get('old_val')
            if new_entity and old_entity and new_entity['water_amount'] != old_entity['water_amount']:
                return True
        return False

    def get_daily_water(self, *, user_id: str, water_date: date) -> WaterIntakeEntity:
        document_id = self.get_doc_id(user_id, str(water_date))
        with self.connection as c:
            result = self.r.table(self.table).get(document_id).run(c)
        if result:
            return WaterIntakeEntity(**result)
        else:
            return WaterIntakeEntity(
                owner_id=user_id,
                date_=water_date,
                water_amount=0
            )

    @staticmethod
    def get_doc_id(user_id: str, date_: str):
        return f"{user_id}::{date_}"
