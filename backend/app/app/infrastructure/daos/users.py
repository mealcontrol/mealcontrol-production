from typing import List, Optional

from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from rethinkdb import RethinkDB

from app import config
from app.domain.authentication.security import get_password_hash
from app.domain.entities import UserUpdate, UserInDB, UserCreate
from app.domain.entities.food.measure import WaterMeasureEntity
from app.domain.interfaces.daos import IUserDAO
from app.infrastructure.db_connection import DBConnection
from app.infrastructure.utils import generate_new_id


class UserDAO(IUserDAO):

    def __init__(self):
        self.r = RethinkDB()
        self.connection = DBConnection(self.r)
        self.table = 'users'

    def create(self, *, user_in: UserCreate) -> UserInDB:
        new_document_id = generate_new_id()
        password_hash = get_password_hash(user_in.password)
        user = UserInDB(**user_in.dict(),
                        hashed_password=password_hash,
                        id=new_document_id,
                        )
        user_json = jsonable_encoder(user)

        with self.connection as c:
            result = self.r.table(self.table).insert(user_json).run(c)

        if result.get('inserted', 0) > 0:
            return user
        raise HTTPException(status_code=400, detail=f"User with email {user_in.email} wasn't created")

    def get(self, *, id_: str) -> Optional[UserInDB]:
        with self.connection as c:
            user = self.r.table(self.table).get(id_).run(c)
        if user:
            return UserInDB(**user)
        return None

    def get_by_email(self, *, email: str) -> Optional[UserInDB]:
        with self.connection as c:
            founded_users = self.r.table(self.table).filter({'email': email}).limit(1).run(c)
        if founded_users.items:
            return UserInDB(**founded_users.items[0])

    def update(self, *, user_id: str, user_in: UserUpdate) -> Optional[UserInDB]:
        user_dict = user_in.dict(skip_defaults=True)
        if user_in.password:
            password_hash = get_password_hash(user_in.password)
            user_dict['hashed_password'] = password_hash
        user_json = jsonable_encoder(user_dict)
        with self.connection as c:
            result = self.r.table(self.table).get(user_id).update(user_json, return_changes=True).run(c)
        changes = result.get('changes')
        if changes:
            new_entity = changes[0].get('new_val')
            if new_entity:
                return UserInDB(**new_entity)
        return None

    def get_user_list(self, *, skip=0, limit=100) -> List[UserInDB]:
        with self.connection as c:
            result = self.r.table(self.table).skip(skip).limit(limit).run(c)
        return [UserInDB(**user) for user in result]

    def search(self, *, query_string: str, skip=0, limit=100):
        return []

    def add_intake(self, user_id: str, intake: str) -> List[str]:
        intakes = self.get_intakes(user_id=user_id)
        if intake not in intakes:
            intakes.append(intake)
        else:
            raise HTTPException(status_code=400, detail="Intake has already been added")
        return self.update_intakes(user_id=user_id, intakes=intakes)

    def get_intakes(self, *, user_id: str) -> List[str]:
        user_info = self.get(id_=user_id)
        return user_info.intakes or config.USER_DEFAULT_INTAKES_LIST

    def update_intakes(self, *, user_id: str, intakes: List[str]) -> List[str]:
        with self.connection as c:
            result = self.r.table(self.table).get(user_id).update(dict(intakes=intakes)).run(c)
        if result.get('replaced', 0) > 0:
            return intakes
        raise HTTPException(status_code=400, detail=f"Intakes were not updated")

    def create_water_measure(self, *, user_id: str, measure: WaterMeasureEntity) -> WaterMeasureEntity:
        measures = self.get_water_measures(user_id=user_id)
        if measure.name in [_measure.name for _measure in measures]:
            raise HTTPException(status_code=400, detail="Measure already exists")
        measures.append(measure)
        self._update_water_measures(user_id=user_id, measures=measures)

        return measure

    def get_water_measures(self, *, user_id: str) -> List[WaterMeasureEntity]:
        user_info = self.get(id_=user_id)
        return user_info.water_measures or [WaterMeasureEntity(**measure)
                                            for measure in config.USER_DEFAULT_WATER_MEASURES_LIST]

    def update_water_measure(self, *, user_id: str, measure_name: str,
                             measure: WaterMeasureEntity) -> WaterMeasureEntity:
        measures = self.get_water_measures(user_id=user_id)
        if measure_name not in [_measure.name for _measure in measures]:
            raise HTTPException(status_code=400, detail=f"Water measure with name {measure_name} does not exists")
        for _measure in measures:
            if _measure.name == measure_name:
                _measure = measure
        self._update_water_measures(user_id=user_id, measures=measures)

        return measure

    def delete_water_measure(self, *, user_id: str, measure_name: str) -> bool:
        measures = self.get_water_measures(user_id=user_id)
        object_to_delete = None
        for _measure in measures:
            if _measure.name == measure_name:
                object_to_delete = _measure

        if object_to_delete:
            measures.remove(object_to_delete)

        try:
            self._update_water_measures(user_id=user_id, measures=measures)
            return True
        except HTTPException:
            return False

    def get_water_measure(self, *, user_id: str, measure_name: str) -> Optional[WaterMeasureEntity]:
        measures = self.get_water_measures(user_id=user_id)
        if measure_name not in [_measure.name for _measure in measures]:
            raise HTTPException(status_code=400, detail=f"Water measure with name {measure_name} does not exists")
        for _measure in measures:
            if _measure.name == measure_name:
                return _measure
        return None

    def _update_water_measures(self, user_id: str, measures: List[WaterMeasureEntity]) -> List[WaterMeasureEntity]:
        with self.connection as c:
            result = self.r.table(self.table).get(user_id)\
                .update(dict(water_measures=jsonable_encoder(measures))).run(c)
        if result.get('replaced', 0) > 0:
            return measures
        raise HTTPException(status_code=400, detail=f"Water measures were not updated")
