from datetime import date, timedelta
from typing import List

from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from rethinkdb import RethinkDB

from app.domain.entities.intakes import IntakeListEntity, IntakeInDBEntity
from app.domain.interfaces.daos import IIntakeDAO
from app.infrastructure.db_connection import DBConnection


class IntakeDAO(IIntakeDAO):

    def __init__(self):
        self.r = RethinkDB()
        self.connection = DBConnection(self.r)
        self.table = "intakes"

    def create(self, *, user_id: str, intakes: IntakeListEntity) -> IntakeListEntity:
        intake_doc_id = self.get_doc_id(user_id, str(intakes.date_))
        intakes_db = IntakeInDBEntity(**intakes.dict())
        data = jsonable_encoder(intakes_db)
        data['id'] = intake_doc_id
        data['date_'] = self.r.time(intakes.date_.year, intakes.date_.month, intakes.date_.day, 'Z')
        with self.connection as c:
            result = self.r.table(self.table).insert(data).run(c)
        if result.get("inserted", 0) > 0:
            return intakes
        raise HTTPException(status_code=400, detail="Intakes weren't created")

    def get(self, *, user_id: str, intake_date: date) -> IntakeListEntity:
        intake_doc_id = self.get_doc_id(user_id, str(intake_date))
        with self.connection as c:
            result = self.r.table(self.table).get(intake_doc_id).run(c)
        if result:
            return IntakeListEntity(**result)
        return None

    def get_user_intakes_in_range(self, *, user_id: str,
                                  intake_date_from: date, intake_date_to: date) -> List[IntakeListEntity]:
        intake_date_to += timedelta(days=1)
        with self.connection as c:
            result = self.r.table(self.table).filter(
                lambda intake: intake['date_'].during(
                    self.r.time(intake_date_from.year, intake_date_from.month, intake_date_from.day, 'Z'),
                    self.r.time(intake_date_to.year, intake_date_to.month, intake_date_to.day, 'Z')
                ) & intake['owner_id'] == user_id
            ).run(c)

        if result:
            return [IntakeListEntity(**intake) for intake in result]
        return []

    def update(self, *, user_id: str, intakes: IntakeListEntity) -> IntakeListEntity:
        doc_id = self.get_doc_id(user_id, str(intakes.date_))
        with self.connection as c:
            result = self.r.table(self.table)\
                .get(doc_id)\
                .update(intakes.dict(skip_defaults=True), return_changes=True)\
                .run(c)
        changes = result.get('changes')
        if changes:
            new_entity = changes[0].get('new_val')
            if new_entity:
                return IntakeListEntity(**new_entity)
        raise HTTPException(status_code=400, detail=f"Intake wasn't updated")

    @staticmethod
    def get_doc_id(user_id: str, date_: str):
        return f"{user_id}::{date_}"
