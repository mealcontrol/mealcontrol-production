import uuid
from typing import TypeVar

from pydantic import BaseModel

PydanticModel = TypeVar("PydanticModel", bound=BaseModel)


def generate_new_id():
    return str(uuid.uuid4())
