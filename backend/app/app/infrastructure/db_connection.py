from app import config


class DBConnection:
    def __init__(self, r):
        self.r = r
        self.creds = dict(
            host=config.DB_HOST,
            port=config.DB_PORT,
            db=config.DB_NAME
        )

    def __enter__(self):
        self.connection = self.r.connect(**self.creds)
        return self.connection

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()
