FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

# Dependencies for Couchbase
RUN wget -O - http://packages.couchbase.com/ubuntu/couchbase.key | apt-key add -
RUN echo "deb http://packages.couchbase.com/ubuntu bionic bionic/main" > /etc/apt/sources.list.d/couchbase.list
RUN apt-get update && apt-get install -y libcouchbase-dev libcouchbase2-bin build-essential


ARG env=prod


COPY ./app /app

RUN pip install -r ./requirements/production.txt
