import axios from 'axios';
import {apiUrl} from '@/env';
import {IFoodBase, IFoodCreate, IFoodDetail, IUserProfile, IUserProfileCreate, IUserProfileUpdate} from './interfaces';

function authHeaders(token: string) {
    return {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
}

export const api = {
    async logInGetToken(username: string, password: string) {
        const params = new URLSearchParams();
        params.append('username', username);
        params.append('password', password);

        return axios.post(`${apiUrl}/api/v1/login/access-token`, params);
    },
    async getMe(token: string) {
        return axios.get<IUserProfile>(`${apiUrl}/api/v1/users/me`, authHeaders(token));
    },
    async updateMe(token: string, data: IUserProfileUpdate) {
        return axios.put<IUserProfile>(`${apiUrl}/api/v1/users/me`, data, authHeaders(token));
    },
    async getUsers(token: string) {
        return axios.get<IUserProfile[]>(`${apiUrl}/api/v1/users/`, authHeaders(token));
    },
    async updateUser(token: string, userId: string, data: IUserProfileUpdate) {
        return axios.put(`${apiUrl}/api/v1/users/${userId}`, data, authHeaders(token));
    },
    async createUser(token: string, data: IUserProfileCreate) {
        return axios.post(`${apiUrl}/api/v1/users/`, data, authHeaders(token));
    },
    async passwordRecovery(email: string) {
        return axios.post(`${apiUrl}/api/v1/password-recovery/${email}`);
    },
    async resetPassword(password: string, token: string) {
        return axios.post(`${apiUrl}/api/v1/reset-password/`, {
            new_password: password,
            token,
        });
    },
    async getFoodList(token: string) {
        return axios.get<IFoodBase[]>(`${apiUrl}/api/v1/food/`, authHeaders(token));
    },
    async createFood(token: string, data: IFoodCreate) {
        return axios.post(`${apiUrl}/api/v1/food/`, data, authHeaders(token));
    },
    async getFoodDetail(token: string, data: string) {
        return axios.get<IFoodDetail>(`${apiUrl}/api/v1/food/${data}`, authHeaders(token));
    },
    async updateFood(token: string, foodId: string, food: IFoodDetail) {
        return axios.put(`${apiUrl}/api/v1/food/${foodId}`, food, authHeaders(token));
    },
    async getFoodListToValidate(token: string) {
        const payload = {
            headers: {Authorization: `Bearer ${token}`},
            params: {valid: false},
        };
        return axios.get<IFoodBase[]>(`${apiUrl}/api/v1/food/`, payload);
    },
    async validateFood(token: string, foodId: string, food: IFoodDetail) {
        return axios.put(`${apiUrl}/api/v1/food/${foodId}/validate`, food, authHeaders(token));
    },
};

