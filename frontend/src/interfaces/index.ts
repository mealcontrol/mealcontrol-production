import DateTimeFormat = Intl.DateTimeFormat;

export interface IUserProfile {
    email: string;
    disabled: boolean;
    role: string;
    firstName: string;
    lastName: string;
    id: string;
}

export interface IUserProfileUpdate {
    email?: string;
    firstName?: string;
    lastName?: string;
    password?: string;
    disabled?: boolean;
    role?: string;
}

export interface IUserProfileCreate {
    email: string;
    firstName?: string;
    lastName?: string;
    password?: string;
    disabled?: boolean;
    role?: string;
}

export interface IFoodBase {
    name: string;
    language: string;
    calories: number;
    fats: number;
    proteins: number;
    carbs: number;
    tags: string[];
    ownerEmail: string;
    id: string;
    state: string;
}

export interface IFoodMeasure {
   measureName: string;
   value: number;
}

export interface IFoodNutrient {
    name: string;
    value: number;
}

export interface IFoodMetadata {
    updatedAt: DateTimeFormat;
    priority: number;
    state: string;
}

export interface IFoodCreate {
    name: string;
    language: string;
    calories: number;
    fats: number;
    proteins: number;
    carbs: number;
    tags: string[];
    ownerEmail: string;
    measures: IFoodMeasure[];
    nutrients: IFoodNutrient[];
    barcode?: string;
}

export interface IFoodDetail {
    name: string;
    language: string;
    calories: number;
    fats: number;
    proteins: number;
    carbs: number;
    tags: string[];
    ownerEmail: string;
    id: string;
    measures: IFoodMeasure[];
    nutrients: IFoodNutrient[];
    barcode?: string;
    metadata: IFoodMetadata;
}
