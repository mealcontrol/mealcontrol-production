import { api } from '@/api';
import { ActionContext } from 'vuex';
import {IFoodCreate, IFoodDetail, IUserProfileCreate, IUserProfileUpdate} from '@/interfaces';
import { State } from '../state';
import { AdminState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import {
    commitSetUsers,
    commitSetUser,
    commitSetFoodList,
    commitSetFoodBase, commitRemoveFoodFromValidation, commitSetFoodDetail,
} from './mutations';
import { dispatchCheckApiError } from '../main/actions';
import { commitAddNotification, commitRemoveNotification } from '../main/mutations';

type MainContext = ActionContext<AdminState, State>;

export const actions = {
    async actionGetUsers(context: MainContext) {
        try {
            const response = await api.getUsers(context.rootState.main.token);
            if (response) {
                commitSetUsers(context, response.data);
            }
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionGetFoodList(context: MainContext) {
        try {
            const response = await api.getFoodList(context.rootState.main.token);
            if (response) {
                commitSetFoodList(context, response.data);
            }
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionUpdateUser(context: MainContext, payload: { id: string, user: IUserProfileUpdate }) {
        try {
            const loadingNotification = { content: 'saving', showProgress: true };
            commitAddNotification(context, loadingNotification);
            const response = (await Promise.all([
                api.updateUser(context.rootState.main.token, payload.id, payload.user),
                await new Promise((resolve, reject) => setTimeout(() => resolve(), 500)),
            ]))[0];
            commitSetUser(context, response.data);
            commitRemoveNotification(context, loadingNotification);
            commitAddNotification(context, { content: 'User successfully updated', color: 'success' });
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionCreateUser(context: MainContext, payload: IUserProfileCreate) {
        try {
            const loadingNotification = { content: 'saving', showProgress: true };
            commitAddNotification(context, loadingNotification);
            const response = (await Promise.all([
                api.createUser(context.rootState.main.token, payload),
                await new Promise((resolve, reject) => setTimeout(() => resolve(), 500)),
            ]))[0];
            commitSetUser(context, response.data);
            commitRemoveNotification(context, loadingNotification);
            commitAddNotification(context, { content: 'User successfully created', color: 'success' });
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionCreateFood(context: MainContext, payload: IFoodCreate) {
        try {
            const loadingNotification = { content: 'saving', showProgress: true };
            commitAddNotification(context, loadingNotification);
            const response = (await Promise.all([
                api.createFood(context.rootState.main.token, payload),
                await new Promise((resolve, reject) => setTimeout(() => resolve(), 500)),
            ]))[0];
            commitSetFoodBase(context, response.data);
            commitRemoveNotification(context, loadingNotification);
            commitAddNotification(context, { content: 'Food successfully created', color: 'success' });
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionUpdateFood(context: MainContext, payload: {id: string, food: IFoodDetail}) {
        try {
            const loadingNotification = { content: 'saving', showProgress: true };
            commitAddNotification(context, loadingNotification);
            const response = (await Promise.all([
                api.updateFood(context.rootState.main.token, payload.id, payload.food),
                await new Promise((resolve, reject) => setTimeout(() => resolve(), 500)),
            ]))[0];
            commitSetFoodBase(context, response.data);
            commitRemoveNotification(context, loadingNotification);
            commitAddNotification(context, { content: 'Food successfully updated', color: 'success' });
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionGetFoodToValidate(context: MainContext) {
        try {
            if (context.state.foodListToValidate.length === 0) {
                const response = await api.getFoodListToValidate(context.rootState.main.token);
                if (response) {
                    await commitSetFoodList(context, response.data);
                }
            }
            if (context.state.foodListToValidate.length > 0) {
                return await actions.actionGetFoodDetail(context, {id: context.state.foodListToValidate[0].id});
            }
            return undefined;
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionValidateFood(context: MainContext, payload: {id: string, food: IFoodDetail}) {
        try {
            const loadingNotification = { content: 'saving', showProgress: true };
            commitAddNotification(context, loadingNotification);
            const response = (await Promise.all([
                api.validateFood(context.rootState.main.token, payload.id, payload.food),
                await new Promise((resolve, reject) => setTimeout(() => resolve(), 500)),
            ]))[0];
            commitSetFoodBase(context, response.data);
            commitRemoveFoodFromValidation(context, response.data);
            commitRemoveNotification(context, loadingNotification);
            commitAddNotification(context, { content: 'Food successfully validated', color: 'success' });
        } catch (error) {
            await dispatchCheckApiError(context, error);
        }
    },
    async actionGetFoodDetail(context: MainContext, payload: {id: string}) {
        try {
            const response = await api.getFoodDetail(context.rootState.main.token, payload.id);
            if (response) {
                await commitSetFoodDetail(context, response.data);
            }
        } catch (error) {
            await dispatchCheckApiError(context, error);
            await commitSetFoodDetail(context, undefined);
        }
    },
};

const { dispatch } = getStoreAccessors<AdminState, State>('');

export const dispatchCreateUser = dispatch(actions.actionCreateUser);
export const dispatchGetUsers = dispatch(actions.actionGetUsers);
export const dispatchUpdateUser = dispatch(actions.actionUpdateUser);
export const dispatchGetFoodList = dispatch(actions.actionGetFoodList);
export const dispatchCreateFood = dispatch(actions.actionCreateFood);
export const dispatchUpdateFood = dispatch(actions.actionUpdateFood);
export const dispatchGetFoodToValidate = dispatch(actions.actionGetFoodToValidate);
export const dispatchValidateFood = dispatch(actions.actionValidateFood);
export const dispatchGetFoodDetail = dispatch(actions.actionGetFoodDetail);

