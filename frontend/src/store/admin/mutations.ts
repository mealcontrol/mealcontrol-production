import {IFoodBase, IFoodDetail, IUserProfile} from '@/interfaces';
import { AdminState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const mutations = {
    setUsers(state: AdminState, payload: IUserProfile[]) {
        state.users = payload;
    },
    setUser(state: AdminState, payload: IUserProfile) {
        const users = state.users.filter((user: IUserProfile) => user.id !== payload.id);
        users.push(payload);
        state.users = users;
    },
    setFoodList(state: AdminState, payload: IFoodBase[]) {
        state.foodList = payload;
    },
    setFoodBase(state: AdminState, payload: IFoodBase) {
        const foodList = state.foodList.filter((food: IFoodBase) => food.id !== payload.id);
        foodList.push(payload);
        state.foodList = foodList;
    },
    removeFoodFromValidation(state: AdminState, payload: IFoodBase) {
        const foodToValidate = state.foodListToValidate.filter((food: IFoodBase) => food.id !== payload.id);
        state.foodListToValidate = foodToValidate;
    },
    setFoodDetail(state: AdminState, payload: IFoodDetail | undefined) {
        state.foodDetail = payload;
    },
};

const { commit } = getStoreAccessors<AdminState, State>('');

export const commitSetUser = commit(mutations.setUser);
export const commitSetUsers = commit(mutations.setUsers);
export const commitSetFoodList = commit(mutations.setFoodList);
export const commitSetFoodBase = commit(mutations.setFoodBase);
export const commitRemoveFoodFromValidation = commit(mutations.removeFoodFromValidation);
export const commitSetFoodDetail = commit(mutations.setFoodDetail);


