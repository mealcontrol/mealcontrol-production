import { AdminState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const getters = {
    adminUsers: (state: AdminState) => state.users,
    adminOneUser: (state: AdminState) => (userId: string) => {
        const filteredUsers = state.users.filter((user) => user.id === userId);
        if (filteredUsers.length > 0) {
            return { ...filteredUsers[0] };
        }
    },
    adminFoodList: (state: AdminState) => state.foodList,
    adminFoodToValidateList: (state: AdminState) => state.foodListToValidate,
    adminFoodDetail: (state: AdminState) => state.foodDetail,
};

const { read } = getStoreAccessors<AdminState, State>('');

export const readAdminOneUser = read(getters.adminOneUser);
export const readAdminUsers = read(getters.adminUsers);
export const readAdminFoodList = read(getters.adminFoodList);
export const readAdminFoodToValidateList = read(getters.adminFoodToValidateList);
export const readAdminFoodDetail = read(getters.adminFoodDetail);
