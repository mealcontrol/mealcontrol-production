import {IFoodBase, IFoodDetail, IUserProfile} from '@/interfaces';

export interface AdminState {
    users: IUserProfile[];
    foodList: IFoodBase[];
    foodListToValidate: IFoodBase[];
    foodDetail: IFoodDetail | undefined;
}
